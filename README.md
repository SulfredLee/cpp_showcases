
## cpp_showcases
This is a project to record all my tools, learning materials in C++.
This is also a project showing how I set up the CI/CD pipeline on gitlab for automatically build, test, develop a C++ project to AWS K8s cluster.

## How to set up the development environment
```bash
$ cd dockerEnv
$ ./BuildImageDev.sh
$ cd dev
$ ./start_dev_container.sh
```

## How to build and run the examples
After started the dev container

```bash
$ cd scripts
$ ./CCMake_Debug.sh
$ ../debug
$ ninja
```

Then you can run any example you want.
