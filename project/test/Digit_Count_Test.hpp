#include <vector>
#include <array>

#include "Digit_Count_10.hpp"

TEST_F(cpp_showcases_Test, Digit_Count_10_Test)
{
    uint64_t v = 1;
    Digit_Count_10 dc10;
    for (int i = 0; i < 16; i++)
    {
        EXPECT_EQ(i+1, dc10.Digits10_Basecase(v));
        EXPECT_EQ(i+1, dc10.Digits10_Step_4(v));
        EXPECT_EQ(i+1, dc10.Digits10_Step_5(v));
        EXPECT_EQ(i+1, dc10.Digits10_Step_6(v));
        EXPECT_EQ(i+1, dc10.Digits10_Step_7(v));
        v *= 10;
    }
}
