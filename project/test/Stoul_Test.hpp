#include <string>
#include <vector>
#include <array>
#include <cstdlib>

#include "Stoul.hpp"

TEST_F(cpp_showcases_Test, Stoul_Random)
{
    const std::array<std::string, 20> s_a =
        {
            "12345678912345678911"
            , "1234567891234567891"
            , "123456789123456789"
            , "12345678912345678"
            , "1234567891234567"
            , "123456789123456"
            , "12345678912345"
            , "1234567891234"
            , "123456789123"
            , "12345678912"
            , "1234567891"
            , "123456789"
            , "12345678"
            , "1234567"
            , "123456"
            , "12345"
            , "1234"
            , "123"
            , "12"
            , "1"
        };
    Stoul ss;
    for (const auto &ele : s_a)
    {
        EXPECT_EQ(std::stoull(ele)
                  , ss.Atoui_Base(ele.c_str(), ele.c_str() + ele.length()));
        EXPECT_EQ(std::stoull(ele)
                  , ss.Atoui_001(ele.c_str(), ele.c_str() + ele.length()));
        EXPECT_EQ(std::stoull(ele)
                  , ss.Atoui_002(ele.c_str(), ele.c_str() + ele.length()));
    }
}
