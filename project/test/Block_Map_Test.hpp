#include "block_map.hpp"

TEST_F(cpp_showcases_Test, Block_Map_Random)
{
    bool is_found_error = false;
    int total_test = 100;
    int cur_test_count = 0;
    std::vector<std::pair<int, char>> random_output;
    while (!is_found_error && total_test > cur_test_count)
    {
        cur_test_count++;
        if (cur_test_count % 20 == 0)
            std::cout << "current test case: " << cur_test_count << std::endl;
        random_output = Generate_Random_Outputs(500, 1, 300000, 'A', 'Z');

        auto random_input = Generate_Random_Input(random_output);
        // insert into map
        block_map<int, char> bm('A');
        for (const auto &[begin_key, end_key, val] : random_input)
            bm.assign(begin_key, end_key, val);

        // check
        for (size_t i = 1; i < random_output.size(); i++)
        {
            const auto &[prev_key, prev_val] = random_output[i-1];
            const auto &[cur_key, cur_val] = random_output[i];

            for (int j = prev_key; j < cur_key; j++)
            {
                if (bm[j] != prev_val)
                {
                    std::cout << "Error expected: " << prev_val
                              << " in index: " << j
                              << " but get: " << bm[j] << std::endl;
                    is_found_error = true;
                    break;
                }
            }
            if (is_found_error)
                break;
        }
        if (is_found_error)
        {
            std::cout << "Expected output:" << std::endl;
            for (const auto &[begin_key, val] : random_output)
                std::cout << begin_key << " " << val << std::endl;
            std::cout << std::endl;

            std::cout << "Random input:" << std::endl;
            auto random_input = Generate_Random_Input(random_output);
            for (const auto &[begin_key, end_key, val] : random_input)
                std::cout << begin_key << " " << end_key << " " << val << std::endl;
            std::cout << std::endl;

            std::cout << "Final we get:" << std::endl;
            bm.print();

            EXPECT_EQ(true, false); // end test
        }
    }
}

TEST_F(cpp_showcases_Test, Block_Map_TrivialRange)
{
    block_map<TestKeyType, TestValueType> m('A');
    m.assign(1, 10, 'B');
    EXPECT_TRUE(m[0] == 'A');
    for (int i = 1; i < 10; i++)
    {
        EXPECT_TRUE(m[i] == 'B');
    }
    EXPECT_TRUE(m[10] == 'A');
}

TEST_F(cpp_showcases_Test, Block_Map_TrivialTwoRange)
{
    block_map<TestKeyType, TestValueType> m('A');
    m.assign(1, 3, 'B');
    m.assign(6, 8, 'C');
    EXPECT_TRUE(m[0] == 'A');
    EXPECT_TRUE(m[1] == 'B');
    EXPECT_TRUE(m[2] == 'B');
    EXPECT_TRUE(m[3] == 'A');
    EXPECT_TRUE(m[4] == 'A');
    EXPECT_TRUE(m[5] == 'A');
    EXPECT_TRUE(m[6] == 'C');
    EXPECT_TRUE(m[7] == 'C');
    EXPECT_TRUE(m[8] == 'A');
}

TEST_F(cpp_showcases_Test, Block_Map_FloatKey)
{
    block_map<TestFloatKeyType, TestValueType> m('A');
    m.assign(1.f, 5.f, 'B');
    EXPECT_TRUE(m[0.f] == 'A');
    EXPECT_TRUE(m[.999999999f] == 'B');
    EXPECT_TRUE(m[1.f] == 'B');
    EXPECT_TRUE(m[4.999f] == 'B');
    EXPECT_TRUE(m[5.f] == 'A');

}
