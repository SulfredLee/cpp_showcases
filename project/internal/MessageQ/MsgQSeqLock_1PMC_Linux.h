//--------------------------------------------- Readme
// This structure only support single-producer and multi-consumer
//--------------------------------------------- Readme

#ifndef MSG_Q_SEQ_LOCK_1PMC_LINUX_H
#define MSG_Q_SEQ_LOCK_1PMC_LINUX_H

#include <vector>
#include <sstream>
#include <atomic>

#include "Seqlock.h"
#include "MsgQBase.h"

template<typename T>
class MsgQSeqLock_1PMC_Linux : public MsgQBase<T>
{
 public:
    MsgQSeqLock_1PMC_Linux(const size_t& total_q)
    {
        m_inIndex.store(0);
        m_ringBuf.resize(MAX_MSG_NUM);
        m_workers.resize(total_q);
    }
    ~MsgQSeqLock_1PMC_Linux()
    {
    }

    // override MsgQBase<T>
    bool AddMsg(std::shared_ptr<T>& msg);
    // override MsgQBase<T>
    bool AddMsg(std::shared_ptr<T>&& msg);
    // override MsgQBase<T>
    void GetMsg(std::shared_ptr<T>& msg, const size_t& idx);
    // override MsgQBase<T>
    int GetMsgNum();
    // override MsgQBase<T>
    void Flush();
    // override MsgQBase<T>
    std::string GetQName();
    // override MsgQBase<T>
    std::string GetQDetails();
 private:
    class MsgQ_Worker
    {
    public:
        MsgQ_Worker()
        {
            m_read_idx = 0;
        }
        ~MsgQ_Worker() {}
    public:
        size_t m_read_idx;
    };
 private:
    Seqlock<size_t> m_inIndex;
    std::vector<std::shared_ptr<T> > m_ringBuf;
    std::vector<MsgQ_Worker> m_workers;
};

// override MsgQBase<T>
template<typename T>
bool MsgQSeqLock_1PMC_Linux<T>::AddMsg(std::shared_ptr<T>& msg)
{
    m_ringBuf[m_inIndex.load()] = std::move(msg);
    m_inIndex.store(((m_inIndex.load() + 1) & (MAX_MSG_NUM - 1))); // mod operation // get next index
    return true;
}

// override MsgQBase<T>
template<typename T>
bool MsgQSeqLock_1PMC_Linux<T>::AddMsg(std::shared_ptr<T>&& msg)
{
    m_ringBuf[m_inIndex.load()] = std::move(msg);
    m_inIndex.store(((m_inIndex.load() + 1) & (MAX_MSG_NUM - 1))); // mod operation // get next index
    return true;
}

// override MsgQBase<T>
template<typename T>
void MsgQSeqLock_1PMC_Linux<T>::GetMsg(std::shared_ptr<T>& msg, const size_t& idx)
{
    if (idx >= m_workers.size())
        return;

    if (m_workers[idx].m_read_idx != m_inIndex.load())
    {
        msg = m_ringBuf[m_workers[idx].m_read_idx];
        m_workers[idx].m_read_idx = ((m_workers[idx].m_read_idx + 1) & (MAX_MSG_NUM - 1)); // mod operation
    }
    else
    {
        return;
    }
}

// override MsgQBase<T>
template<typename T>
int MsgQSeqLock_1PMC_Linux<T>::GetMsgNum()
{
    return 0;
}

// override MsgQBase<T>
template<typename T>
void MsgQSeqLock_1PMC_Linux<T>::Flush()
{
}

// override MsgQBase<T>
template<typename T>
std::string MsgQSeqLock_1PMC_Linux<T>::GetQName()
{
    return "MsgQSeqLock_1PMC_Linux";
}

// override MsgQBase<T>
template<typename T>
std::string MsgQSeqLock_1PMC_Linux<T>::GetQDetails()
{
    // std::string details;
    // for (auto& q : m_vec_q)
    //     details += q.GetQDetails() + " ";

    return "Not implemented";
}
#endif
