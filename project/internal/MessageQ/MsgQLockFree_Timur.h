#ifndef MSGQLOCKFREE_TIMUR_H
#define MSGQLOCKFREE_TIMUR_H

#include <atomic>

template<typename T, size_t size>
    struct LockFreeQueue
    {
        bool push(const T& newElement)
        {
            auto oldWritePos = writePos.load();
            auto newWritePos = getPositionAfter(oldWritePos);

            if (newWritePos == readPos.load())
                return false;

            ringBuffer[oldWritePos] = newElement;

            writePos.store(newWritePos);
            return true;
        }

        bool pop(T& returnedElement)
        {
            auto oldWritePos = writePos.load();
            auto oldReadPos = readPos.load();

            if (oldWritePos == oldReadPos)
                return false;

            returnedElement = std::move(ringBuffer[oldReadPos]);

            readPos.store(getPositionAfter(oldReadPos));
            return true;
        }

    private:
        static constexpr size_t getPositionAfter(size_t pos) noexcept
        {
            return ++pos == ringBufferSize ? 0 : pos;
        }

        static constexpr size_t ringBufferSize = size + 1;
        std::array<T, ringBufferSize> ringBuffer;
        std::atomic<size_t> readPos = 0, writePos = 0;
    };

#endif
