#ifndef MSG_Q_LOCK_FREE_BOOST_H
#define MSG_Q_LOCK_FREE_BOOST_H
#include <vector>
#include <sstream>
#include <atomic>
#include <iostream>
#include <boost/thread/thread.hpp>
#include <boost/lockfree/queue.hpp>

#include "MsgQBase.h"

template<typename T>
class MsgQLockFree_Boost : public MsgQBase<T>
{
 public:
    MsgQLockFree_Boost()
    {
        m_queue = std::make_unique<boost::lockfree::queue<T> >(MAX_MSG_NUM);
        // m_queue.reserve(MAX_MSG_NUM);
    }
    ~MsgQLockFree_Boost()
    {
    }

    // override MsgQBase<T>
    bool AddMsg(std::shared_ptr<T>& msg);
    // override MsgQBase<T>
    bool AddMsg(std::shared_ptr<T>&& msg);
    // override MsgQBase<T>
    void GetMsg(std::shared_ptr<T>& msg);
    // override MsgQBase<T>
    int GetMsgNum();
    // override MsgQBase<T>
    void Flush();
    // override MsgQBase<T>
    std::string GetQName();
    // override MsgQBase<T>
    std::string GetQDetails();
 private:
    std::unique_ptr<boost::lockfree::queue<T> > m_queue;
};

// override MsgQBase<T>
template<typename T>
bool MsgQLockFree_Boost<T>::AddMsg(std::shared_ptr<T>& msg)
{
    m_queue->push(std::move(*msg));
    return true;
}

// override MsgQBase<T>
template<typename T>
bool MsgQLockFree_Boost<T>::AddMsg(std::shared_ptr<T>&& msg)
{
    m_queue->push(*msg);
    return true;
}

// override MsgQBase<T>
template<typename T>
void MsgQLockFree_Boost<T>::GetMsg(std::shared_ptr<T>& msg)
{
    T tmpMsg;
    m_queue->pop(tmpMsg);
    msg = std::make_shared<T>(std::move(tmpMsg));
}

// override MsgQBase<T>
template<typename T>
int MsgQLockFree_Boost<T>::GetMsgNum()
{
    return 0;
}

// override MsgQBase<T>
template<typename T>
void MsgQLockFree_Boost<T>::Flush()
{
}

// override MsgQBase<T>
template<typename T>
std::string MsgQLockFree_Boost<T>::GetQName()
{
    return "MsgQLockFree_Boost";
}

// override MsgQBase<T>
template<typename T>
std::string MsgQLockFree_Boost<T>::GetQDetails()
{
    return "No information";
}
#endif
