//--------------------------------------------- Readme
// This structure only support single-producer and multi-consumer
//--------------------------------------------- Readme

#ifndef MSG_Q_LOCK_FREE_1PMC_LINUX_H
#define MSG_Q_LOCK_FREE_1PMC_LINUX_H

#include <vector>

#include "MsgQBase.h"
#include "MsgQLockFree_Linux.h"

template<typename T>
class MsgQLockFree_1PMC_Linux : public MsgQBase<T>
{
 public:
    MsgQLockFree_1PMC_Linux(const size_t& total_q)
    {
        m_next_q = 0;
        m_vec_q.resize(total_q);
    }
    ~MsgQLockFree_1PMC_Linux()
    {
    }

    // override MsgQBase<T>
    bool AddMsg(std::shared_ptr<T>& msg);
    // override MsgQBase<T>
    bool AddMsg(std::shared_ptr<T>&& msg);
    // override MsgQBase<T>
    void GetMsg(std::shared_ptr<T>& msg, const size_t& idx);
    // override MsgQBase<T>
    int GetMsgNum();
    // override MsgQBase<T>
    void Flush();
    // override MsgQBase<T>
    std::string GetQName();
    // override MsgQBase<T>
    std::string GetQDetails();
 private:
    size_t m_next_q;
    std::vector<MsgQLockFree_Linux<int> > m_vec_q;
};

// override MsgQBase<T>
template<typename T>
bool MsgQLockFree_1PMC_Linux<T>::AddMsg(std::shared_ptr<T>& msg)
{
    ++m_next_q %= m_vec_q.size();
    return m_vec_q[m_next_q].AddMsg(msg);
}

// override MsgQBase<T>
template<typename T>
bool MsgQLockFree_1PMC_Linux<T>::AddMsg(std::shared_ptr<T>&& msg)
{
    ++m_next_q %= m_vec_q.size();
    return m_vec_q[m_next_q].AddMsg(std::move(msg));
}

// override MsgQBase<T>
template<typename T>
void MsgQLockFree_1PMC_Linux<T>::GetMsg(std::shared_ptr<T>& msg, const size_t& idx)
{
    if (idx >= m_vec_q.size())
        return;

    m_vec_q[idx].GetMsg(msg);
}

// override MsgQBase<T>
template<typename T>
int MsgQLockFree_1PMC_Linux<T>::GetMsgNum()
{
    int sum = 0;
    // for (const MsgQLockFree_Linux& q : m_vec_q)
    for (auto& q : m_vec_q)
    {
        sum += q.GetMsgNum();
    }

    return sum;
}

// override MsgQBase<T>
template<typename T>
void MsgQLockFree_1PMC_Linux<T>::Flush()
{
    for (auto& q : m_vec_q)
        q.Flush();
}

// override MsgQBase<T>
template<typename T>
std::string MsgQLockFree_1PMC_Linux<T>::GetQName()
{
    return "MsgQLockFree_1PMC_Linux";
}

// override MsgQBase<T>
template<typename T>
std::string MsgQLockFree_1PMC_Linux<T>::GetQDetails()
{
    // std::string details;
    // for (auto& q : m_vec_q)
    //     details += q.GetQDetails() + " ";

    return "Not implemented";
}
#endif
