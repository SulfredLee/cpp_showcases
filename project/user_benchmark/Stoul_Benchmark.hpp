#ifndef STOUL_BENCHMARK_HPP
#define STOUL_BENCHMARK_HPP

#include <string>
#include <vector>
#include <array>
#include <cstdlib>
#include <iostream>

#include "Stoul.hpp"

void Stoul_Benchmark_Main()
{
    std::cout << "batch" << "\t"
              << "iteration" << "\t"
              << "num_digit" << "\t"
              << "base_case" << "\t"
              << "001_case" << "\t"
              << "002_case" << std::endl;
    int batch = 50;
    int iteration = 10000;

    const std::array<std::string, 20> s_a =
        {
            "12345678912345678911"
            , "1234567891234567891"
            , "123456789123456789"
            , "12345678912345678"
            , "1234567891234567"
            , "123456789123456"
            , "12345678912345"
            , "1234567891234"
            , "123456789123"
            , "12345678912"
            , "1234567891"
            , "123456789"
            , "12345678"
            , "1234567"
            , "123456"
            , "12345"
            , "1234"
            , "123"
            , "12"
            , "1"
        };

    // batch minimum benchmark
    int i = s_a.size()-1;
    for (const auto &ele : s_a)
    {
        std::cout << batch << "\t" << iteration << "\t" << i+1 << "\t";
        Stoul ss;
        uint64_t run_time = Run_Batch_Min_Benchmark(batch, iteration, [&] () -> void
        {
            ss.Atoui_Base(ele.c_str(), ele.c_str() + ele.length());
        }
            , [&] () -> void
            {
                char const * ss = ele.c_str() + ele.length();
                ss++;
            });
        std::cout << run_time << "\t";

        run_time = Run_Batch_Min_Benchmark(batch, iteration, [&] () -> void
        {
            ss.Atoui_001(ele.c_str(), ele.c_str() + ele.length());
        }
            , [&] () -> void
            {
                char const * ss = ele.c_str() + ele.length();
                ss++;
            });
        std::cout << run_time << "\t";

        run_time = Run_Batch_Min_Benchmark(batch, iteration, [&] () -> void
        {
            ss.Atoui_002(ele.c_str(), ele.c_str() + ele.length());
        }
            , [&] () -> void
            {
                char const * ss = ele.c_str() + ele.length();
                ss++;
            });
        std::cout << run_time << std::endl;

        i--;
    }
}

// Define benchmark
static void BM_Stoul(benchmark::State& state) {
    for (auto _ : state)
    {
        Stoul_Benchmark_Main();
    }
}
BENCHMARK(BM_Stoul);

#endif
