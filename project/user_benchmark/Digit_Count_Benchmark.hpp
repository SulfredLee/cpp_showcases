#ifndef DIGIT_COUNT_BENCHMARK_HPP
#define DIGIT_COUNT_BENCHMARK_HPP

#include <iostream>
#include <string>
#include <sstream>
#include <vector>

#include "Digit_Count_10.hpp"

void Digit_Count_Benchmark_Main()
{
    std::cout << "batch" << "\t"
              << "iteration" << "\t"
              << "num_digit" << "\t"
              << "base_case" << "\t"
              << "case_4" << "\t"
              << "case_5" << "\t"
              << "case_6" << "\t"
              << "case_7" << std::endl;
    std::vector<u_int64_t> vv {1, 12, 123, 1234, 12345, 123456, 1234567, 12345678
                               , 123456789, 1234567891, 12345678912, 123456789123, 1234567891234, 12345678912345
                               , 123456789123456, 1234567891234567, 12345678912345678};
    int batch = 50;
    int iteration = 10000;
    // batch minimum benchmark
    for (size_t i = 0; i < vv.size(); i++)
    {
        Digit_Count_10 dc;
        uint64_t run_time = Run_Batch_Min_Benchmark(batch, iteration, [&] () -> void
        {
            dc.Digits10_Basecase(vv[i]);
        }
            , [&] () -> void
            {
                vv[i];
            });
        std::cout << batch << "\t" << iteration << "\t" << i+1 << "\t";
        std::cout << run_time << "\t";

        run_time = Run_Batch_Min_Benchmark(batch, iteration, [&] () -> void
        {
            dc.Digits10_Step_4(vv[i]);
        }
            , [&] () -> void
            {
                vv[i];
            });
        std::cout << run_time << "\t";

        run_time = Run_Batch_Min_Benchmark(batch, iteration, [&] () -> void
        {
            dc.Digits10_Step_5(vv[i]);
        }
            , [&] () -> void
            {
                vv[i];
            });
        std::cout << run_time << "\t";

        run_time = Run_Batch_Min_Benchmark(batch, iteration, [&] () -> void
        {
            dc.Digits10_Step_6(vv[i]);
        }
            , [&] () -> void
            {
                vv[i];
            });
        std::cout << run_time << "\t";

        run_time = Run_Batch_Min_Benchmark(batch, iteration, [&] () -> void
        {
            dc.Digits10_Step_7(vv[i]);
        }
            , [&] () -> void
            {
                vv[i];
            });
        std::cout << run_time << std::endl;
    }
}

// Define benchmark
static void BM_Digit_Count(benchmark::State& state) {
    for (auto _ : state)
    {
        Digit_Count_Benchmark_Main();
    }
}
BENCHMARK(BM_Digit_Count);

#endif
