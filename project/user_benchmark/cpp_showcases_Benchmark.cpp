// https://stackoverflow.com/a/48110959/2358836 --- in case you get warning from google benchmark for cpu scaling
// sudo cpupower frequency-set --governor performance
// ./mybench
// sudo cpupower frequency-set --governor powersave

#include <benchmark/benchmark.h>
#include <chrono>

template <typename T1, typename T2>
uint64_t Run_Batch_Min_Benchmark(int b
                                 , int n
                                 , T1 fun
                                 , T2 fun_empty)
{
    std::chrono::time_point<std::chrono::system_clock> start_time_point, end_time_point;
    std::chrono::duration<double> fun_elapsed_seconds, loop_elapsed_seconds;
    uint64_t min_time = std::numeric_limits<uint64_t>::max();
    for (int i = 0; i < b; i++)
    {
        start_time_point = std::chrono::system_clock::now();
        for (int j = 0; j < n; j++)
        {
            fun();
        }
        fun_elapsed_seconds = std::chrono::system_clock::now() - start_time_point;

        // time the for loop to remove the overhead noise
        start_time_point = std::chrono::system_clock::now();
        // for (int j = 0; j < n; j++) { benchmark::DoNotOptimize(j); } // { asm volatile(""); } // volatile the variable so that cpp O3 is not going to optimiz it
        for (int j = 0; j < n; j++) { benchmark::DoNotOptimize(j); fun_empty(); } // call another function to subtraction overhead noise
        loop_elapsed_seconds = std::chrono::system_clock::now() - start_time_point;

        uint64_t cur_run_time = static_cast<uint64_t>(fun_elapsed_seconds.count() * 1000 * 1000 + 0.5); // get nano second
        cur_run_time -= static_cast<uint64_t>(loop_elapsed_seconds.count() * 1000 * 1000 + 0.5); // remove loop overhead
        if (min_time > cur_run_time)
            min_time = cur_run_time;
    }
    return min_time;
}

// #include "Digit_Count_Benchmark.hpp"
#include "Stoul_Benchmark.hpp"

BENCHMARK_MAIN();
