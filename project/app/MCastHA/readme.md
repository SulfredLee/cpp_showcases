## Start dev container
```bash
$ cd ./dockerEnv/dev
$ ./start_dev_container.sh
```

## Example
```bash
$ ./app/MCastHA/MCastHA
20230414_122722_875 [MSG]           MCastArbitrator::          SendArbitration:    70,125269,Start thread
20230414_122722_876 [MSG]           MCastArbitrator::       ReceiveArbitration:   106,125270,Start thread
20230414_122723_376 [MSG]           MCastArbitrator::SwitchStatus_LongTimeNoUpdate:   211,125270,IN
20230414_122723_376 [MSG]           MCastArbitrator::              PrintStatus:   174,125270,Status updated Raise hand
20230414_122723_626 [MSG]           MCastArbitrator::              PrintStatus:   174,125270,Status updated Primary
20230414_122723_875 [MSG]                    Sender::                     Main:    42,125268,Send success! count: 1 instance: 1
20230414_122723_875 [MSG]           MCastArbitrator::          SendArbitration:    70,125276,Start thread
20230414_122723_875 [MSG]           MCastArbitrator::       ReceiveArbitration:   106,125277,Start thread
20230414_122723_876 [MSG]           MCastArbitrator::              PrintStatus:   174,125277,Status updated Secondary
20230414_122724_875 [MSG]                    Sender::                     Main:    42,125268,Send success! count: 2 instance: 1
20230414_122725_876 [MSG]                    Sender::                     Main:    42,125268,Send success! count: 3 instance: 1
20230414_122726_876 [MSG]                    Sender::                     Main:    42,125268,Send success! count: 4 instance: 1
20230414_122728_129 [MSG]           MCastArbitrator::SwitchStatus_LongTimeNoUpdate:   211,125270,IN
20230414_122728_129 [MSG]           MCastArbitrator::              PrintStatus:   174,125270,Status updated Raise hand
20230414_122728_130 [MSG]           MCastArbitrator::SwitchStatus_LongTimeNoUpdate:   211,125277,IN
20230414_122728_130 [MSG]           MCastArbitrator::              PrintStatus:   174,125277,Status updated Raise hand
20230414_122728_378 [MSG]           MCastArbitrator::              PrintStatus:   174,125270,Status updated Secondary
20230414_122728_378 [MSG]           MCastArbitrator::              PrintStatus:   174,125277,Status updated Primary
20230414_122728_876 [MSG]                    Sender::                     Main:    42,125275,Send success! count: 5 instance: 2
20230414_122729_876 [MSG]                    Sender::                     Main:    42,125275,Send success! count: 6 instance: 2
20230414_122730_876 [MSG]                    Sender::                     Main:    42,125275,Send success! count: 7 instance: 2
20230414_122731_877 [MSG]                    Sender::                     Main:    42,125275,Send success! count: 8 instance: 2
20230414_122733_131 [MSG]           MCastArbitrator::SwitchStatus_LongTimeNoUpdate:   211,125270,IN
20230414_122733_131 [MSG]           MCastArbitrator::              PrintStatus:   174,125270,Status updated Raise hand
20230414_122733_131 [MSG]           MCastArbitrator::SwitchStatus_LongTimeNoUpdate:   211,125277,IN
20230414_122733_131 [MSG]           MCastArbitrator::              PrintStatus:   174,125277,Status updated Raise hand
20230414_122733_381 [MSG]           MCastArbitrator::              PrintStatus:   174,125270,Status updated Primary
20230414_122733_381 [MSG]           MCastArbitrator::              PrintStatus:   174,125277,Status updated Secondary
20230414_122733_877 [MSG]                    Sender::                     Main:    42,125268,Send success! count: 11 instance: 1
20230414_122734_877 [MSG]                    Sender::                     Main:    42,125268,Send success! count: 12 instance: 1
20230414_122735_877 [MSG]                    Sender::                     Main:    42,125268,Send success! count: 13 instance: 1
20230414_122736_877 [MSG]                    Sender::                     Main:    42,125268,Send success! count: 14 instance: 1
```
