#ifndef WORD_LADDER_HPP
#define WORD_LADDER_HPP

#include <iostream>
#include <string>
#include <unordered_set>
#include <random>
#include <iterator>
#include <tuple>
#include <queue>
#include <vector>

using namespace std;

class Word_Ladder_Solution
{
public:
    Word_Ladder_Solution();
    ~Word_Ladder_Solution();

    int Get_Answer(const string &start_word
                   , const string &end_word
                   , const unordered_set<string> &dict
                   , bool is_debug);
    tuple<unordered_set<string>, int> Prepare_Test_Case(const string &start_word
                                                        , const string &end_word
                                                        , int additional_words
                                                        , bool is_has_answer);
    void Print_Case(const string &start_word
                    , const string &end_word
                    , const unordered_set<string> &dict);
    void Run_Test(int num_test, int word_size);
private:
    string Get_Random_Word(int word_size);
    vector<string> Expand_Stage(const string &cur_word, const unordered_set<string> &dict);
};

Word_Ladder_Solution::Word_Ladder_Solution()
{
}

Word_Ladder_Solution::~Word_Ladder_Solution()
{
}

int Word_Ladder_Solution::Get_Answer(const string &start_word
                                     , const string &end_word
                                     , const unordered_set<string> &dict
                                     , bool is_debug)
{
    if (dict.count(end_word) == 0\
        || dict.count(start_word) == 0\
        || start_word.size() != end_word.size())
        return -1; // no solution

    int ladder_steps = 0;
    unordered_set<string> seen; // prevent cycle
    queue<string> cur_level, next_level;
    cur_level.push(start_word);
    seen.insert(start_word);

    if (is_debug) cout << "Next level: ";
    while (cur_level.size() > 0)
    {
        string cur_word = cur_level.front();
        cur_level.pop();

        if (cur_word == end_word)
            return ladder_steps; // found solution

        for (const auto &next_word : Expand_Stage(cur_word, dict))
        {
            if (seen.count(next_word) != 0)
                continue;
            next_level.push(next_word);
            seen.insert(next_word);

            if (is_debug) cout << next_word << " ";
        }

        // swithc to next level
        if (cur_level.size() == 0)
        {
            swap(cur_level, next_level);
            ladder_steps++;
            if (is_debug)
            {
                cout << endl;
                cout << "Next level: ";
            }
        }
    }

    return -1; // no solution
}

tuple<unordered_set<string>, int>
Word_Ladder_Solution::Prepare_Test_Case(const string &start_word
                                        , const string &end_word
                                        , int additional_words
                                        , bool is_has_answer)
{
    unordered_set<string> result;
    int ladder_steps = 0;

    // generate a valid ladder
    string ladder_word = start_word;
    result.insert(ladder_word);
    for (size_t i = 0; i < end_word.size(); i++)
    {
        if (ladder_word[i] != end_word[i])
        {
            ladder_word[i] = end_word[i];
            ladder_steps++;
        }
        result.insert(ladder_word);
    }

    // insert extra words
    for (int i = 0; i < additional_words; i++)
    {
        result.insert(Get_Random_Word(end_word.size()));
    }

    // confirm the result has no answer
    if (!is_has_answer)
    {
        result.erase(end_word);
    }

    return {result, ladder_steps};
}


void Word_Ladder_Solution::Print_Case(const string &start_word
                                      , const string &end_word
                                      , const unordered_set<string> &dict)
{
    cout << "start_word: " << start_word
         << " end_word: " << end_word
         << endl;

    cout << "the dict" << endl;
    copy(dict.begin(), dict.end(), ostream_iterator<string>(cout, " "));
    cout << endl;
}

void Word_Ladder_Solution::Run_Test(int num_test, int word_size)
{
    bool is_with_answer = true;
    for (int j = 0; j < 2; j++)
    {
        for (int i = 0; i < num_test; i++)
        {
            string start_word = Get_Random_Word(word_size);
            string end_word = Get_Random_Word(word_size);
            auto ret_test_case = Prepare_Test_Case(start_word
                                                   , end_word
                                                   , 100
                                                   , is_with_answer);
            auto &[dict, expected_steps] = ret_test_case;
            if (is_with_answer == false)
                expected_steps = -1;
            int calculated_steps = Get_Answer(start_word, end_word, dict, false);
            if (calculated_steps != expected_steps)
            {
                cout << "We have case" << endl;
                Print_Case(start_word, end_word, dict);
                cout << endl;
                cout << "Expected steps: " << expected_steps
                     << " We get: " << calculated_steps << endl;

                return;
            }
        }
        is_with_answer = false;
    }
}

string Word_Ladder_Solution::Get_Random_Word(int word_size)
{
    static random_device r;
    static default_random_engine el(r());
    static uniform_int_distribution<char> ran_char('a', 'z');

    string extra_word; extra_word.reserve(word_size);
    for (int j = 0; j < word_size; j++)
    {
        extra_word.push_back(ran_char(el));
    }

    return extra_word;
}

vector<string>
Word_Ladder_Solution::Expand_Stage(const string &cur_word
                                   , const unordered_set<string> &dict)
{
    vector<string> result; result.reserve(26 * cur_word.size());

    string next_word = cur_word;
    for (size_t i = 0; i < next_word.size(); i++)
    {
        char old_char = next_word[i];
        for (char c = 'a'; c <= 'z'; c++)
        {
            if (c == old_char)
                continue;
            next_word[i] = c;

            if (dict.count(next_word) == 0)
                continue;
            result.push_back(next_word);
        }
        next_word[i] = old_char;
    }

    return result;
}

void Demo_Word_Ladder()
{
    Word_Ladder_Solution wls;
    string start_word = "mod";
    string end_word = "cap";

    auto ret_test_case = wls.Prepare_Test_Case(start_word, end_word, 10, true);
    auto &[dict, expected_steps] = ret_test_case;
    wls.Print_Case(start_word, end_word, dict);
    int answer_step = wls.Get_Answer(start_word, end_word, dict, true);
    cout << endl;
    cout << "Answer: " << answer_step << endl;
}
#endif
