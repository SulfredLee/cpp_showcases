## Algo and Data structure examples

In this project to collect a list of examples.

### Content
| example_name                                        | learn_from                                             | create_time | file                        | key_skills         |
|:---------------------------------------------------:|:------------------------------------------------------:|:-----------:|-----------------------------|:------------------:|
| [block map](#block-map)                             | stack overflow                                         | 2023/09/11  | block_map.hpp               | std::map           |
| [word ladder](#word-ladder)                         | leetcode                                               | 2023/09/17  | word_ladder.hpp             | BFS                |
| [q_rsqrt](#quick-reverse-square-root)               | What is low latency C++ - Timur Doumler                | 2023/09/18  | q_rsqrt.hpp                 |                    |
| [digit_count_10](#digit-counting-10)                | [youtube](https://www.youtube.com/watch?v=vrfYLlR8X8k) | 2023/09/25  | Digit_Count_10.hpp          | micro-optimization |
| [stoul](#stoul)                                     | [youtube](https://www.youtube.com/watch?v=9tvbz8CSI8M) | 2023/09/26  | Stoul.hpp                   | micro-optimization |
| [android_unlock_patterns](#android-unlock-patterns) | leetcode                                               | 2023/10/03  | Android_Unlock_Patterns.hpp | DFS                |
| [word_break_2](#word-break-2)                       | leetcode                                               | 2023/10/14  | Word_Break_2.hpp            | Dynamic            |
|                                                     |                                                        |             |                             |                    |


### block map
##### Description

This is an intesting data structure found from [stack overflow](https://stackoverflow.com/questions/54068482/where-exactly-does-my-code-not-adhere-to-the-specification-of-the-key-and-value)

##### Files
- block_map.hpp
- cpp_showcases_Test.cpp
  - Included a list of test case for this structure

##### Outputs
```Bash
Start Demo_Block_Map
Insert: 3, 6, B
3 B
6 A
Insert: 2, 5, C
2 C
5 B
6 A
Insert: 4, 7, A
2 C
4 A
```

### word ladder
##### Description

Reference from [leetcode pdf](https://github.com/SulfredLee/leetcode/blob/master/C%2B%2B/leetcode-cpp.pdf)

##### Outputs
```Bash
start_word: mod end_word: cap
the dict
oka cod hbn dre guf cad cap iwg olx prz mod sfu tzy goo
Next level: cod
Next level: cad
Next level: cap
Next level:
Answer: 3
```

### quick reverse square root
##### Description

Reference from [wiki page](https://en.wikipedia.org/wiki/Fast_inverse_square_root)

##### Outputs
```Bash
Reverse square root of 16: 0.249577
```

### digit counting 10
##### Description

Reference from [youtube](https://www.youtube.com/watch?v=vrfYLlR8X8k)
It is a question rasied by Andrei Alexandrescu to demonstrate how to do speed measuring. I borrow this idea and create a benchmark case to confirm my understanding.

##### Outputs
```Bash
$ ./app/Algo_Data_Structure/Algo_Data_Structure --app digit-count-10
Hello World from app: Algo_Data_Structure

app: digit-count-10
10 has 2 digits
100 has 3 digits
```

### stoul
##### Description

It is a micro-optimization example rasied by Andrei Alexandrescu [youtube](https://www.youtube.com/watch?v=9tvbz8CSI8M)

##### Outputs
```Bash
$ ./app/Algo_Data_Structure/Algo_Data_Structure --app stoul
Hello World from app: Algo_Data_Structure

app: stoul
Convert 12345, 12345
```

### android unlock patterns
##### Description

##### Outputs
```Bash
$ ./app/Algo_Data_Structure/Algo_Data_Structure --app android-unlock-patterns
Hello World from app: Algo_Data_Structure

app: android-unlock-patterns
Test finished. Not issue found.
```

### word break 2
##### Description

##### Outputs
```Bash
$ ./app/Algo_Data_Structure/Algo_Data_Structure --app word-break-2
Hello World from app: Algo_Data_Structure

app: word-break-2
test_input: llcpshdjslfiokllcpshsdfsdfskdhosdfsdfdogwerufcvjslsdfllcpshcvjslsandcatswerufdogcatssand
test_input: llcpshdjslfiokllcpshsdfsdfskdhosdfsdfdogwerufcvjslsdfllcpshcvjslsandcatswerufdogcatssandzzzzzzzz
Get answer: llcpsh djslfiok llcpsh sdf sdf skdho sdf sdf dog weruf cvjsl sdf llcpsh cvjsl sand cats weruf dog cats sand
Finish test case 2 found no issues.
```
