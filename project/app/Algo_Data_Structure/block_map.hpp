#ifndef BLOCK_MAP_HPP
#define BLOCK_MAP_HPP

#include <iostream>
#include <map>
#include <set>
#include <string>
#include <vector>
#include <tuple>
#include <algorithm>
#include <cassert>
#include <random>

template<typename K, typename V>
class block_map {
    friend void BlockMapTest();
    V m_valBegin;
    std::map<K,V> m_map;
public:
    block_map(V const& val)
        : m_valBegin(val)
    {}

    void print() const
    {
        for (const auto &[k, v] : m_map)
        {
            std::cout << k << " " << v << std::endl;
        }
    }

    // Assign value val to block [keyBegin, keyEnd)
    void assign( K const& keyBegin, K const& keyEnd, V const& val ) {
        if (!(keyBegin < keyEnd)) return; // return if empty block
        // debug
        // std::cout << "assign keyBegin: " << keyBegin
        //           << " keyEnd: " << keyEnd
        //           << " val: " << val << std::endl;

        // initial for empty map
        if (m_map.size() == 0)
        {
            const auto it = m_map.insert(m_map.begin(), {keyBegin, val});
            m_map.insert(it, {keyEnd, m_valBegin});
            return;
        }

        auto next_begin_it = m_map.upper_bound(keyBegin);
        auto cur_begin_it = next_begin_it == m_map.begin() ? m_map.begin() : std::prev(next_begin_it);
        bool is_begin_key_equal = next_begin_it != m_map.begin() && !(cur_begin_it->first < keyBegin) ? true : false;
        V cur_begin_val = next_begin_it == m_map.begin() ? m_valBegin : cur_begin_it->second;
        auto prev_begin_it = cur_begin_it == m_map.begin() ? m_map.begin() : std::prev(cur_begin_it);
        V prev_begin_val = cur_begin_it == m_map.begin() ? m_valBegin : prev_begin_it->second;

        auto next_end_it = m_map.lower_bound(keyEnd);
        bool is_end_key_equal = next_end_it == m_map.end() ? false : !(keyEnd < next_end_it->first);
        V next_end_val = next_end_it == m_map.end() ? m_valBegin : next_end_it->second;
        V cur_end_val = next_end_it == m_map.begin() ? m_valBegin : std::prev(next_end_it)->second;

        // erase [begin current block, next end block)
        auto erase_end_it = is_end_key_equal && val == next_end_val ? std::next(next_end_it) : next_end_it;
        auto erase_begin_it = is_begin_key_equal ? cur_begin_it : next_begin_it;

        // debug
        // std::cout << "prev_begin_val: " << prev_begin_val << std::endl;
        // std::cout << "cur_begin_val: " << cur_begin_val << std::endl;
        // std::cout << "cur_end_val: " << cur_end_val << std::endl;
        // std::cout << "val: " << val << std::endl;
        // std::cout << "is_begin_key_equal: " << is_begin_key_equal << std::endl;
        // std::cout << "is_end_key_equal: " << is_end_key_equal << std::endl;
        // std::cout << "next_end_it: " << next_end_it->first << " " << next_end_it->second << std::endl;
        // std::cout << "erase_end_it: " << erase_end_it->first << " " << erase_end_it->second << std::endl;

        auto erase_it = m_map.erase(erase_begin_it, erase_end_it);

        // insert back
        auto insert_it = erase_it;
        if ((!is_begin_key_equal && val != cur_begin_val) || (is_begin_key_equal && prev_begin_val != val))
        {
            insert_it = m_map.insert(insert_it, {keyBegin, val});
            insert_it++;
        }
        if (val != cur_end_val && !is_end_key_equal)
        {
            insert_it = m_map.insert(insert_it, {keyEnd, cur_end_val});
        }
    }

    V const& operator[]( K const& key ) const {
        auto it=m_map.upper_bound(key);
        if(it==m_map.begin()) {
            return m_valBegin;
        } else {
            return (--it)->second;
        }
    }
};

struct TestKeyType
{
    unsigned int val;
    constexpr TestKeyType(unsigned int val) : val(val) {}
    constexpr bool operator<(const TestKeyType& other) const { return val < other.val; }
};

namespace std {
    template<> class numeric_limits<TestKeyType> {
    public:
        static constexpr TestKeyType lowest() { return TestKeyType(numeric_limits<unsigned int>::lowest()); }
        //static constexpr TestKeyType lowest() { return TestKeyType(-250); }
    };
}

using TestValueType = char;

struct TestFloatKeyType
{
    float val;

    TestFloatKeyType() = default;

    TestFloatKeyType(float val) : val(val) {}
    bool operator< (TestFloatKeyType other) const
    {
        return other.val - val > 1.e-4f;
    }
};

namespace std {
    template<> class numeric_limits<TestFloatKeyType> {
    public:
        static TestFloatKeyType lowest() { return TestFloatKeyType(numeric_limits<float>::lowest()); }
    };
}

std::vector<std::pair<int, char>>
Generate_Random_Outputs(int num_of_inputs
                        , int begin_key
                        , int end_key
                        , const char &begin_val
                        , const char &end_val)
{
    std::random_device r;
    std::default_random_engine el(r());
    std::uniform_int_distribution<int> ran_key(begin_key, end_key);
    std::uniform_int_distribution<char> ran_val(begin_val, end_val);

    // first input
    std::set<int> first_input;
    while (first_input.size() <= (size_t)num_of_inputs) // extra number for block ending
    {
        first_input.insert(ran_key(el));
    }
    // final_output
    std::vector<std::pair<int, char>> final_output;
    char last_val = begin_val;
    for (const auto &key : first_input)
    {
        char cur_val = ran_val(el);
        while (cur_val == last_val)
            cur_val = ran_val(el);

        // confirm a different value generated
        final_output.push_back({key, cur_val});
        last_val = cur_val;
    }
    final_output.back().second = begin_val;
    return final_output;
}

std::vector<std::tuple<int, int, char>>
Generate_Random_Input(const std::vector<std::pair<int, char>>& expected_output)
{
    std::set<int> seen;
    std::vector<std::tuple<int, int, char>> random_input;

    std::random_device r;
    std::default_random_engine el(r());
    std::uniform_int_distribution<size_t> ran_idx(0, expected_output.size()-2);
    while (seen.size() < expected_output.size() - 1)
    {
        size_t cur_idx = ran_idx(el);
        while (seen.count(cur_idx))
            cur_idx = ran_idx(el);

        auto next_level_it = seen.lower_bound(cur_idx);
        auto prev_level_it = next_level_it == seen.begin() ? seen.begin() : std::prev(next_level_it);
        // handle begin
        int rand_begin_start = next_level_it == seen.begin()
            ? expected_output[cur_idx].first : expected_output[(*prev_level_it) + 1].first;
        int rand_begin_end = expected_output[cur_idx].first;

        // handle end
        int rand_end_start = expected_output[cur_idx + 1].first;
        int rand_end_end = next_level_it == seen.end()
            ? expected_output.back().first : expected_output[*next_level_it].first;

        // std::cout << "generate for " << expected_output[cur_idx].first << " " << expected_output[cur_idx].second
        //           << " " << rand_begin_start << "/" << rand_begin_end << " " << rand_end_start << "/" << rand_end_end << std::endl;
        // generate random
        std::uniform_int_distribution<int> ran_begin(rand_begin_start, rand_begin_end);
        std::uniform_int_distribution<int> ran_end(rand_end_start, rand_end_end);
        random_input.push_back({ran_begin(el), ran_end(el), expected_output[cur_idx].second});

        // update seen
        seen.insert(cur_idx);
    }

    return random_input;
}

void Demo_Block_Map()
{
    std::cout << "Start Demo_Block_Map" << std::endl;

    bool is_found_error = false;
    std::vector<std::pair<int, char>> random_output;
    while (!is_found_error)
    {
        random_output = Generate_Random_Outputs(500, 1, 300000, 'A', 'Z');

        auto random_input = Generate_Random_Input(random_output);
        // insert into map
        block_map<int, char> bm('A');
        for (const auto &[begin_key, end_key, val] : random_input)
            bm.assign(begin_key, end_key, val);

        // check
        for (size_t i = 1; i < random_output.size(); i++)
        {
            const auto &[prev_key, prev_val] = random_output[i-1];
            const auto &[cur_key, cur_val] = random_output[i];

            for (int j = prev_key; j < cur_key; j++)
            {
                if (bm[j] != prev_val)
                {
                    std::cout << "Error expected: " << prev_val
                              << " in index: " << j
                              << " but get: " << bm[j] << std::endl;
                    is_found_error = true;
                    break;
                }
            }
            if (is_found_error)
                break;
        }
        if (is_found_error)
        {
            std::cout << "Expected output:" << std::endl;
            for (const auto &[begin_key, val] : random_output)
                std::cout << begin_key << " " << val << std::endl;
            std::cout << std::endl;

            std::cout << "Random input:" << std::endl;
            auto random_input = Generate_Random_Input(random_output);
            for (const auto &[begin_key, end_key, val] : random_input)
                std::cout << begin_key << " " << end_key << " " << val << std::endl;
            std::cout << std::endl;

            std::cout << "Final we get:" << std::endl;
            bm.print();
        }
    }
}

void Demo_Block_Map_02()
{
    block_map<int, char> bm('A');
    bm.assign(22, 28, 'C');
    bm.assign(6, 17, 'U');
    bm.assign(10, 21, 'B');
    bm.assign(9, 13, 'Y');

    bm.print();
    std::cout << std::endl;

    bm.assign(18, 22, 'A');

    bm.print();
}

void Demo_Block_Map_01()
{
    block_map<int, char> bm('A');
    bm.assign(10, 18, 'K');
    bm.assign(8, 10, 'O');
    bm.assign(21, 25, 'S');
    bm.assign(15, 24, 'Q');

    bm.print();
    std::cout << std::endl;

    bm.assign(12, 17, 'S');

    bm.print();
}
#endif
