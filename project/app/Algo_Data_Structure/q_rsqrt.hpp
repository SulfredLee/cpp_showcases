#ifndef Q_RSQRT_HPP
#define Q_RSQRT_HPP

#include <iostream>
#include <bit>
#include <cstdint>

// constexpr float Q_Rsqrt(float y)
// {
//     constexpr auto threehalfs = 1.5f;
//     // constexpr auto x2 = y * 0.5f;
//     auto x2 = y * 0.5f;
//
//     auto i = std::bit_cast<int>(y); // bit_cast<> not supported in GCC 9
//     i = 0x5f3759df - (i >> 1);
//
//     y = std::bit_cast<float>(i);
//     y = y * (threehalfs - ( x2 * y * y));
//
//     return y;
// }

float Q_Rsqrt(float number)
{
    int i;
    float x2, y;
    const float threehalfs = 1.5f;

    x2 = number * 0.5f;
    y = number;

    i = *(int*) &y;
    i = 0x5f3759df - (i >> 1);

    y = *(float *) &i;
    y = y * (threehalfs - (x2 * y * y));

    return y;
}

void Demo_Q_Rsqrt()
{
    std::cout << "Reverse square root of 16: " << Q_Rsqrt(16) << std::endl;
}
#endif
