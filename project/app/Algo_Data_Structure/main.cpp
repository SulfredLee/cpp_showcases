
#include <iostream>
using namespace std;

#include "block_map.hpp"
#include "word_ladder.hpp"
#include "q_rsqrt.hpp"
#include "Digit_Count_10.hpp"
#include "Stoul.hpp"
#include "Android_Unlock_Patterns.hpp"
#include "Word_Break_2.hpp"

#include "Input_Parser.hpp"

int main (int argc, char *argv[])
{
    std::cout << "Hello World from app: Algo_Data_Structure" << std::endl;
    std::cout << std::endl;

    // define input arguments
    struct Opts
    {
        string app_selection{};
    };
    auto parser = CmdOpts<Opts>::Create({{"--app", &Opts::app_selection}});
    auto cmd_p = parser->parse(argc, argv);

    // confirm we get a correct inpuut
    if (cmd_p.app_selection == "")
    {
        cerr << "Usage:" << endl;
        cerr << "your_app --app <app-selection>" << endl;

        return 1;
    }
    cout << "app: " << cmd_p.app_selection << endl;

    // execute the selected app
    if (cmd_p.app_selection == "block-map")
        Demo_Block_Map();
    else if (cmd_p.app_selection == "word-ladder")
        Demo_Word_Ladder();
    else if (cmd_p.app_selection == "q-rsqrt")
        Demo_Q_Rsqrt();
    else if (cmd_p.app_selection == "digit-count-10")
        Demo_Digit_Count_10();
    else if (cmd_p.app_selection == "stoul")
        Demo_Stoul();
    else if (cmd_p.app_selection == "android-unlock-patterns")
        Demo_Android_Patterns();
    else if (cmd_p.app_selection == "word-break-2")
        Demo_Word_Break_2();
    else
        cout << "app-selection: " << cmd_p.app_selection << " not found. Stop processing." << endl;

    return 0;
}
