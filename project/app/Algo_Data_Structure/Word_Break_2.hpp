#ifndef WORD_BREAK_2_HPP
#define WORD_BREAK_2_HPP

#include <iostream>
#include <unordered_set>
#include <string>
#include <tuple>
#include <vector>
#include <random>

using namespace std;

class Word_Break_2
{
public:
    Word_Break_2() {}
    ~Word_Break_2() {}

    vector<vector<string>> Get_Answer(const string &input_s
                                      , const unordered_set<string> &dict);
    vector<tuple<string, int>> Prepare_Test_Case(const unordered_set<string> &dict
                                                 , int test_count
                                                 , int word_count);
    void Run_Test();
private:
    template<typename It>
    void dfs(It start
             , It end
             , const unordered_set<string> &dict
             , vector<string> &path
             , vector<vector<string>> &result);
};

vector<vector<string>>
Word_Break_2::Get_Answer(const string &input_s
                         , const unordered_set<string> &dict)
{
    vector<vector<string>> result;
    vector<string> path;
    dfs(input_s.begin(), input_s.end(), dict, path, result);
    return result;
}

vector<tuple<string, int>>
Word_Break_2::Prepare_Test_Case(const unordered_set<string> &dict
                                , int test_count
                                , int word_count)
{
    vector<string> dict_v {dict.begin(), dict.end()};
    random_device r;
    default_random_engine el(r());
    uniform_int_distribution<int> ran_key(0, dict_v.size() - 1);

    vector<tuple<string, int>> result;

    string expect_no_solution = "";
    for (int i = 0; i < test_count; i++)
    {
        string test_input = "";
        for (int j = 0; j < word_count; j++)
        {
            test_input += dict_v[ran_key(el)];
        }
        result.push_back({test_input, 1});

        expect_no_solution = test_input;

        cout << "test_input: " << test_input << endl;
    }

    expect_no_solution += "zzzzzzzz";
    cout << "test_input: " << expect_no_solution << endl;
    result.push_back({expect_no_solution, 0});

    return result;
}

void Word_Break_2::Run_Test()
{
    unordered_set<string> dict =
        {
            "cat"
            , "cats"
            , "and"
            , "sand"
            , "dog"
            , "sdf"
            , "weruf"
            , "cvjsl"
            , "djslfiok"
            , "xjd"
            , "skdho"
            , "llcpsh"
        };

    auto eles = Prepare_Test_Case(dict, 1, 20);

    for (const auto &[test_inputs, expected_counts] : eles)
    {
        auto ans = Get_Answer(test_inputs, dict);
        for (const auto &one_ans : ans)
        {
            cout << "Get answer: ";
            for (const auto &ans_part : one_ans)
                cout << ans_part << " ";
            cout << endl;
        }
        if (ans.size() == 0 && expected_counts == 0)
            continue;
        else if (ans.size() == 0 && expected_counts != 0)
        {
            cerr << "Expected: " << test_inputs
                 << " but we have no answer" << endl;

            return;
        }
        else
        {
            for (const auto &one_ans : ans)
            {
                string temp_ans = "";
                for (const auto &ans_part : one_ans)
                    temp_ans += ans_part;

                if (temp_ans != test_inputs)
                {
                    cerr << "Expected: " << test_inputs
                         << " but we get wrong answer: " << temp_ans
                         << endl;

                    for (const auto &ans_part : one_ans)
                        cerr << ans_part << " ";
                    cerr << endl;

                    return;
                }
            }
        }
    }

    cout << "Finish test case " << eles.size() << " found no issues." << endl;
}

template<typename It>
void Word_Break_2::dfs(It start
                       , It end
                       , const unordered_set<string> &dict
                       , vector<string> &path
                       , vector<vector<string>> &result)
{
    for (const auto &word : dict)
    {
        string part_word = string(start, start + word.size());
        if (word == part_word)
        {
            path.push_back(word);
            if (start + word.size() == end)
                result.push_back(path);
            else
                dfs(start + word.size(), end, dict, path, result);
            path.pop_back();
        }
    }
}

void Demo_Word_Break_2()
{
    Word_Break_2 wb2;
    wb2.Run_Test();
}
#endif
