#ifndef STOUL_HPP
#define STOUL_HPP
#include <stdexcept>
#include <string>
#include <iostream>

class Stoul
{
public:
    Stoul() {}
    ~Stoul() {}

    uint64_t Atoui_Base(const char *b, const char *e);
    uint64_t Atoui_001(const char *b, const char *e);
    uint64_t Atoui_002(const char *b, const char *e);
private:
    void enforce(bool b);
};

uint64_t Stoul::Atoui_Base(const char *b, const char *e)
{
    enforce(b < e);
    uint64_t result = 0;
    for (; b != e; ++b)
    {
        enforce(*b >= '0' && *b <= '9');
        result = result * 10 + (*b - '0');
    }

    return result;
}

uint64_t Stoul::Atoui_001(const char *b, const char *e)
{
    // we use a table in this solution
    static const uint64_t pow10[20] =
        {
            10000000000000000000UL
            , 1000000000000000000UL
            , 100000000000000000UL
            , 10000000000000000UL
            , 1000000000000000UL
            , 100000000000000UL
            , 10000000000000UL
            , 1000000000000UL
            , 100000000000UL
            , 10000000000UL
            , 1000000000UL
            , 100000000UL
            , 10000000UL
            , 1000000UL
            , 100000UL
            , 10000UL
            , 1000UL
            , 100UL
            , 10UL
            , 1UL
        };
    enforce(b < e);
    uint64_t result = 0;
    auto i = sizeof(pow10) / sizeof(*pow10) - (e - b);
    for (; b != e; ++b)
    {
        enforce(*b >= '0' && *b <= '9');
        result += pow10[i++] * (*b - '0');
    }
    return result;
}

uint64_t Stoul::Atoui_002(const char *b, const char *e)
{
    // we use a table in this solution
    // we reduce the number of comparison
    static const uint64_t pow10[20] =
        {
            10000000000000000000UL
            , 1000000000000000000UL
            , 100000000000000000UL
            , 10000000000000000UL
            , 1000000000000000UL
            , 100000000000000UL
            , 10000000000000UL
            , 1000000000000UL
            , 100000000000UL
            , 10000000000UL
            , 1000000000UL
            , 100000000UL
            , 10000000UL
            , 1000000UL
            , 100000UL
            , 10000UL
            , 1000UL
            , 100UL
            , 10UL
            , 1UL
        };
    enforce(b < e);
    uint64_t result = 0;
    auto i = sizeof(pow10) / sizeof(*pow10) - (e - b);
    for (; b != e; ++b)
    {
        // we can do this because subtraction is injective function --- no two inputs lead to the same output
        // in other word, there is only one possible input when d < 10, and that is all we need
        auto d = uint64_t(*b) - '0';
        if (d >= 10) throw std::range_error("ehm");
        result += pow10[i++] * d;
    }
    return result;
}

void Stoul::enforce(bool b)
{
    if (!b)
        throw std::runtime_error("Error");
}

void Demo_Stoul()
{
    Stoul ss;
    std::string s_i = "12345";
    std::cout << "Convert " << s_i << ", " << ss.Atoui_001(s_i.c_str(), s_i.c_str() + s_i.length()) << std::endl;
}

#endif
