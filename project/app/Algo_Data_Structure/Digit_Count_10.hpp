#ifndef DIGIT_COUNT_10_HPP
#define DIGIT_COUNT_10_HPP

#include <iostream>

class Digit_Count_10
{
public:
    Digit_Count_10() {}
    ~Digit_Count_10() {}

    u_int32_t Digits10_Basecase(u_int64_t v);
    u_int32_t Digits10_Step_4(u_int64_t v);
    u_int32_t Digits10_Step_5(u_int64_t v);
    u_int32_t Digits10_Step_6(u_int64_t v);
    u_int32_t Digits10_Step_7(u_int64_t v);
};

u_int32_t Digit_Count_10::Digits10_Basecase(u_int64_t v)
{
    u_int32_t result = 0;
    do
    {
        ++result;
        v /= 10;
    } while (v);
    return result;
}

u_int32_t Digit_Count_10::Digits10_Step_4(u_int64_t v)
{
    u_int32_t result = 1;
    for (;;)
    {
        if (v < 10) return  result;
        if (v < 100) return result + 1;
        if (v < 1000) return result + 2;
        if (v < 10000) return result + 3;
        v /= 10000U;
        result += 4;
    }
}

u_int32_t Digit_Count_10::Digits10_Step_5(u_int64_t v)
{
    u_int32_t result = 1;
    for (;;)
    {
        if (v < 10) return  result;
        if (v < 100) return result + 1;
        if (v < 1000) return result + 2;
        if (v < 10000) return result + 3;
        if (v < 100000) return result + 4;
        v /= 100000U;
        result += 5;
    }
}

u_int32_t Digit_Count_10::Digits10_Step_6(u_int64_t v)
{
    u_int32_t result = 1;
    for (;;)
    {
        if (v < 10) return  result;
        if (v < 100) return result + 1;
        if (v < 1000) return result + 2;
        if (v < 10000) return result + 3;
        if (v < 100000) return result + 4;
        if (v < 1000000) return result + 5;
        v /= 1000000U;
        result += 6;
    }
}

u_int32_t Digit_Count_10::Digits10_Step_7(u_int64_t v)
{
    u_int32_t result = 1;
    for (;;)
    {
        if (v < 10) return  result;
        if (v < 100) return result + 1;
        if (v < 1000) return result + 2;
        if (v < 10000) return result + 3;
        if (v < 100000) return result + 4;
        if (v < 1000000) return result + 5;
        if (v < 10000000) return result + 6;
        v /= 10000000U;
        result += 7;
    }
}

void Demo_Digit_Count_10()
{
    Digit_Count_10 dc10;
    std::cout << "10 has " << dc10.Digits10_Basecase(10) << " digits" << std::endl;
    std::cout << "100 has " << dc10.Digits10_Basecase(100) << " digits" << std::endl;
}

#endif
