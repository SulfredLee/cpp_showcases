#ifndef ANDROID_UNLOCK_PATTERNS_H
#define ANDROID_UNLOCK_PATTERNS_H
#include <vector>
#include <tuple>
#include <iostream>

using namespace std;

class Android_Unlock_Patterns
{
public:
    Android_Unlock_Patterns() {}
    ~Android_Unlock_Patterns() {}

    tuple<int, vector<vector<int>>> Get_Answer(const int &start_len
                                               , const int &end_len);
    vector<tuple<int, int, int>> Prepare_Test_Case();
    void Run_Test();
private:
    void Get_Answer(const int &start_len, const int &end_len
                    , const int &last_idx, int step, int visited
                    , int &result_count, vector<vector<int>> &result_patterns
                    , vector<int> &path);
    bool Is_Valid(int last_idx, int next_idx, int visited);
};

tuple<int, vector<vector<int>>> Android_Unlock_Patterns::Get_Answer(const int &start_len
                                                                    , const int &end_len)
{
    int result_count = 0;
    vector<vector<int>> result_patterns;
    int visited = 0;
    vector<int> path;
    Get_Answer(start_len, end_len
               , -1, 0, visited
               , result_count, result_patterns, path);
    return {result_count, result_patterns};
}

void Android_Unlock_Patterns::Get_Answer(const int &start_len, const int &end_len
                                         , const int &last_idx, int step, int visited
                                         , int &result_count, vector<vector<int>> &result_patterns
                                         , vector<int> &path)
{
    if (start_len <= step && step < end_len)
    {
        result_count++;
        result_patterns.push_back(path);
    }
    else if (step == end_len)
    {
        result_count++;
        result_patterns.push_back(path);
        return;
    }

    // expand status
    for (int i = 0; i < 9; i++)
    {
        if (Is_Valid(last_idx, i, visited))
        {
            visited |= 1 << i;
            path.push_back(i);
            Get_Answer(start_len, end_len, i, step+1, visited, result_count, result_patterns, path);
            path.pop_back();
            visited &= ~(1 << i);
        }
    }
}

bool Android_Unlock_Patterns::Is_Valid(int last_idx, int next_idx, int visited)
{
    // if already visited
    if (visited & 1 << next_idx)
        return false;
    // if init pattern
    if (last_idx == -1)
        return true;
    // if direct next or long narrow diagonal
    int sum_idx = last_idx + next_idx;
    if ((sum_idx) % 2 == 1)
        return true;
    int mid = sum_idx / 2;
    // if pass through the central - long diagonal
    if (mid == 4)
        return visited & 1 << mid;
    // if next diagonal --- if (not same col) and (not same row)
    if ((last_idx % 3 != next_idx % 3) && (last_idx / 3 != next_idx / 3))
        return true;
    // if jump case
    return visited & 1 << mid;
}

vector<tuple<int, int, int>> Android_Unlock_Patterns::Prepare_Test_Case()
{
    // tuple<start_len, end_len, expected_count
    return vector<tuple<int, int, int>> {
        {1, 1, 9}
        , {2, 2, 56}
        , {3, 3, 320}
        , {2, 3, 376}
    };
}

void Android_Unlock_Patterns::Run_Test()
{
    for (const auto &ele : Prepare_Test_Case())
    {
        const auto &[start_len, end_len, expected_count] = ele;
        tuple<int, vector<vector<int>>> test_result = Get_Answer(start_len, end_len);
        const auto &[test_expected_count, test_expected_patterns] = test_result;

        if (expected_count != test_expected_count)
        {
            cerr << "start_len: " << start_len
                 << " end_len: " << end_len
                 << " expected_count: " << expected_count
                 << " test_expected_count: " << test_expected_count << endl;
            cerr << "Result not match, test failed. Stopped." << endl;

            for (const auto &p_ele : test_expected_patterns)
            {
                for (const auto &p_r_ele : p_ele)
                {
                    cout << p_r_ele << ",";
                }
                cout << endl;
            }

            return;
        }
    }

    cout << "Test finished. Not issue found." << endl;
}

void Demo_Android_Patterns()
{
    Android_Unlock_Patterns aup;
    aup.Run_Test();
}

#endif
