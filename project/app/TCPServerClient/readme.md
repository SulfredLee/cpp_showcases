## Start dev container
```bash
$ cd ./dockerEnv/dev
$ ./start_dev_container.sh
```

## Example
```bash
$ ./app/TCPServerClient/TCPServerClient 
20230414_115551_068 [MSG]                   TCPCast::                   Accept:   121,115166,Received client 192.168.1.104:57016
20230414_115551_068 [MSG]              ServerWorker:    29,115166,Send success! count: 0
20230414_115551_069 [MSG]              ClientWorker:    73,115170,Received count: 0
20230414_115551_069 [MSG]              ClientWorker:    80,115170,Send success! count: 2000
20230414_115551_069 [MSG]              ServerWorker:    43,115166,Received count: 2000 byteRecv: 4
20230414_115551_069 [MSG]              ServerWorker:    29,115166,Send success! count: 1
20230414_115552_069 [MSG]              ClientWorker:    73,115170,Received count: 1
20230414_115552_069 [MSG]              ClientWorker:    80,115170,Send success! count: 2001
20230414_115552_069 [MSG]              ServerWorker:    43,115166,Received count: 2001 byteRecv: 4
20230414_115552_069 [MSG]              ServerWorker:    29,115166,Send success! count: 2
20230414_115553_069 [MSG]              ClientWorker:    73,115170,Received count: 2
20230414_115553_069 [MSG]              ClientWorker:    80,115170,Send success! count: 2002
20230414_115553_069 [MSG]              ServerWorker:    43,115166,Received count: 2002 byteRecv: 4
20230414_115553_069 [MSG]              ServerWorker:    29,115166,Send success! count: 3
20230414_115554_069 [MSG]              ClientWorker:    73,115170,Received count: 3
20230414_115554_070 [MSG]              ClientWorker:    80,115170,Send success! count: 2003
20230414_115554_070 [MSG]              ServerWorker:    43,115166,Received count: 2003 byteRecv: 4
20230414_115554_070 [MSG]              ServerWorker:    29,115166,Send success! count: 4
20230414_115555_070 [MSG]              ClientWorker:    73,115170,Received count: 4
20230414_115555_070 [MSG]              ClientWorker:    80,115170,Send success! count: 2004
20230414_115555_070 [MSG]              ServerWorker:    43,115166,Received count: 2004 byteRecv: 4
20230414_115555_070 [MSG]              ServerWorker:    29,115166,Send success! count: 5
```
