
#include <iostream>
#include <string>
#include <sstream>
#include "stdlib.h" // getenv()

#include <pqxx/pqxx>

std::string GetEnvSafe(const std::string& key, const std::string& default_value)
{
    char *value = getenv(key.c_str());
    return value == NULL ? default_value : std::string(value);
}

int main(int argc, char* argv[])
{
    try
    {
        std::string dbname = GetEnvSafe("DB_NAME", "postgres");
        std::string dbuser = GetEnvSafe("DB_USER", "postgres");
        std::string dbpw = GetEnvSafe("DB_PW", "1234abcd##");
        std::string host = GetEnvSafe("DB_HOST", "0.0.0.0");
        std::string port = GetEnvSafe("DB_PORT", "5432");
        std::string schema = GetEnvSafe("DB_SCHEMA", "refdb");
        std::stringstream connection_code;
        connection_code << "dbname = " << dbname
                        << " user = " << dbuser
                        << " password = " << dbpw
                        << " hostaddr = " << host
                        << " port = " << port;
        std::cout << "Going to connect to: " << dbname << " " << dbuser << "@" << host << ":" << port << std::endl;
        pqxx::connection C(connection_code.str());
        if (C.is_open())
        {
            std::cout << "Opened database successfully: " << C.dbname() << std::endl;

            std::stringstream sql;
            sql << "SELECT c.id, c.current_name, c.ref_data_set from " << schema << ".company as c";
            pqxx::nontransaction N(C);
            pqxx::result R(N.exec(sql.str().c_str()));

            std::cout << "id,current_name,ref_data_set" << std::endl;
            for (pqxx::result::const_iterator c = R.begin(); c != R.end(); ++c)
            {
                std::cout << c[0].as<std::string>() << ","
                          << c[1].as<std::string>() << ","
                          << c[2].as<std::string>() << std::endl;
            }
            std::cout << "Operation done successfully" << std::endl;
        }
        else
        {
            std::cout << "Can't open database" << std::endl;
            return 1;
        }
        C.close();
    }
    catch (const std::exception &e)
    {
        std::cerr << e.what() << std::endl;
        return 1;
    }

    return 0;
}
