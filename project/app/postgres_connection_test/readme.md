## How to run this example
### Start postgresql database
```bash
# start postgresql database --- here we use minikube
$ minikube start --memory 16384 --cpus 8 --disk-size='40000mb' --driver=kvm2
# install or upgrade kube-state-metrics
$ helm upgrade --wait --timeout=1200s --install postgresql --set auth.postgresPassword="1234abcd##" ./postgresql
# port forward so that we can connect to the db outside the minikube
$ kubectl port-forward --namespace default svc/postgresql 5432:5432 &
```
### Start dev container
```bash
$ cd ./dockerEnv/dev
$ ./start_dev_container.sh
```

### Result
```bash
$ ./app/postgres_connection_test/postgres_connection_test
Opened database successfully: postgres
id,current_name,ref_data_set
eq.cmp.1.FSmKCMQnmr3lRm1ra3anU,Pyxis Tankers Inc,qc
eq.cmp.1.Di62_gejFzxcPWVixb8xW,Agilent Technologies Inc,qc
eq.cmp.1.9nnhyp8dMaiscwBTu1l4I,Howmet Aerospace Inc,qc
eq.cmp.1.GbWXWvNRAlFpb54eRD5a1,Alcoa Corp,qc
eq.cmp.1.UfQObuY1QwSGCBXPNGT0q,American Airlines Group Inc,qc
eq.cmp.1.x0jJp53Mq9rAzRYTw4scI,Altisource Asset Management Corp,qc
eq.cmp.1.mWxcBhS5O1mw1r5kxt-ry,Atlantic American Corp,qc
eq.cmp.1.Ocl-D2V9SH1-GGW371Zsa,Applied Optoelectronics Inc,qc
Operation done successfully
```
