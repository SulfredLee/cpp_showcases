## Start dev container
```bash
$ cd ./dockerEnv/dev
$ ./start_dev_container.sh
```

## Example
```bash
$ ./app/DefaultMutexLinux/DefaultMutexLinux
count: 0
^C
```
