
#include <iostream>

#include "DefaultMutex.h"

// usage: ./DefaultMutexLinux

DefaultMutex defaultMutex;

void FooLoop(int thisCount)
{
    DefaultLock lock(defaultMutex);
    if (thisCount < 10)
    {
        std::cout << "count: " << thisCount++ << std::endl;
        FooLoop(thisCount);
    }
    else
        return;
}

int main(int argc, char *argv[])
{
    FooLoop(0);
    return 0;
}
