## Start dev container
```bash
$ cd ./dockerEnv/dev
$ ./start_dev_container.sh
```

## Example
```bash
$ ./app/LifeCycleDemo/LifeCycleDemo
ElemInBase
BaseFoo
ElemInChild
FooA
~FooA
~ElemInChild
~BaseFoo
~ElemInBase
```
