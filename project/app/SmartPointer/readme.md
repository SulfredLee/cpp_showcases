## Start dev container
```bash
$ cd ./dockerEnv/dev
$ ./start_dev_container.sh
```

## Example
```bash
$ ./app/SmartPointer/SmartPointer

Start simple case ==============
Hello from constructor
Hello from Foo
Hello from destructor
Hello from END
Case done ======================

Start inherit case ==============
Hello from BaseClass constructor
Hello from ChildClass constructor
Print from Base Hello from ChildClass Print
Hello from END
Hello from ChildClass Print
Hello from END
Hello from ChildClass destructor
Hello from BaseClass destructor
Case done ======================
```
