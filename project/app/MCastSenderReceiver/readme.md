## Start dev container
```bash
$ cd ./dockerEnv/dev
$ ./start_dev_container.sh
```

## Example
```bash
$ ./app/MCastSenderReceiver/MCastSenderReceiver
20230414_120918_961 [MSG]               MCastSender:    29,119290,Send success! count: 0
20230414_120919_961 [MSG]               MCastSender:    29,119290,Send success! count: 1
20230414_120919_961 [MSG]             MCastReceiver:    62,119295,Received from 192.168.1.104:6000 count: 1
20230414_120920_961 [MSG]               MCastSender:    29,119290,Send success! count: 2
20230414_120920_961 [MSG]             MCastReceiver:    62,119295,Received from 192.168.1.104:6000 count: 2
20230414_120921_961 [MSG]               MCastSender:    29,119290,Send success! count: 3
20230414_120921_961 [MSG]             MCastReceiver:    62,119295,Received from 192.168.1.104:6000 count: 3
20230414_120922_962 [MSG]               MCastSender:    29,119290,Send success! count: 4
20230414_120922_962 [MSG]             MCastReceiver:    62,119295,Received from 192.168.1.104:6000 count: 4
20230414_120923_962 [MSG]               MCastSender:    29,119290,Send success! count: 5
20230414_120923_962 [MSG]             MCastReceiver:    62,119295,Received from 192.168.1.104:6000 count: 5
```
