## Start dev container
```bash
$ cd ./dockerEnv/dev
$ ./start_dev_container.sh
```

## Example
```bash
$ ./app/DailyTimer/DailyTimer
20230415_011838_175 [MSG]                DailyTimer::                     Main:    65,    76,Next alert time: 2023-04-15 01:18:38
Print from non-class 10
20230415_011843_176 [MSG]                DailyTimer::                     Main:    65,    77,Next alert time: 2023-04-15 01:18:43
Print from class 10
```
