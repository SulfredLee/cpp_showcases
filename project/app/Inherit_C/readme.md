## Start dev container
```bash
$ cd ./dockerEnv/dev
$ ./start_dev_container.sh
```

## Example
```bash
$ ./app/Inherit_C/Inherit_C
I am an apple 001
We have 10 apples.
I am an orange 002
```
