## Start dev container
```bash
$ cd ./dockerEnv/dev
$ ./start_dev_container.sh
```

## Example
```bash
$ ./app/CountTimer/CountTimer
Simple Start Stop:
Second: 0 MSecond: 200 NSecond: 200101
Moving Start Stop:
Second: 1 MSecond: 500 NSecond: 500141
Second: 1 MSecond: 1000 NSecond: 1000250
Second: 2 MSecond: 1500 NSecond: 1500360
Time To String:
StartTimeGmt: 2023-04-15 01:57:01 StartTimeLocal: 2023-04-15 09:57:01
StopTimeGmt: 2023-04-15 01:57:02 StopTimeLocal: 2023-04-15 09:57:02
```
