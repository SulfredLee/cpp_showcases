
#include <iostream>
#include "Logger.h"
#include "Dummy.h"

int main (int argc, char *argv[])
{
    Logger::LoggerConfig loggerConfig = Logger::LoggerConfig();
    loggerConfig.logLevel = Logger::LogLevel::DEBUG;
    loggerConfig.logPath = ".";
    loggerConfig.fileSize = 0;
    loggerConfig.fileSizeLimit = 4 * 1024 * 1024; // 4 MByte
    loggerConfig.isToConsole = true;
    loggerConfig.isToFile = false;
    LOGMSG_INIT(loggerConfig);

    LOGMSG_DBG_C("Hello World from app: logger_demo\n");
    LOGMSG_WRN_C("Hello World from app: logger_demo\n");
    LOGMSG_MSG_C("Hello World from app: logger_demo\n");
    LOGMSG_ERR_C("Hello World from app: logger_demo\n");

    LOGMSG_MSG_S_C() << std::endl;
    LOGMSG_MSG_S_C() << "=================================" << std::endl;
    LOGMSG_MSG_S_C() << std::endl;

    LOGMSG_DBG_S_C() << "Hello World from app: logger_demo" << std::endl;
    LOGMSG_WRN_S_C() << "Hello World from app: logger_demo" << std::endl;
    LOGMSG_MSG_S_C() << "Hello World from app: logger_demo" << std::endl;
    LOGMSG_ERR_S_C() << "Hello World from app: logger_demo" << std::endl;

    LOGMSG_MSG_S_C() << std::endl;
    LOGMSG_MSG_S_C() << "=================================" << std::endl;
    LOGMSG_MSG_S_C() << std::endl;

    Dummy dum;
    dum.WhoAmI();

    return 0;
}
