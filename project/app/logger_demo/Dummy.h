#ifndef DUMMY_H
#define DUMMY_H

#include <string>

class Dummy
{
 public:
    Dummy();
    ~Dummy();

    void WhoAmI();
 private:
    std::string m_whoAmI;

};
#endif
