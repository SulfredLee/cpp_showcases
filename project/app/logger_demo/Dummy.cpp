#include "Dummy.h"
#include "Logger.h"

Dummy::Dummy()
{
    LOGMSG_CLASS_NAME("Dummy");
    m_whoAmI = "dummy_001";
}

Dummy::~Dummy()
{
}

void Dummy::WhoAmI()
{
    LOGMSG_DBG("Hello from: %s\n", m_whoAmI.c_str());
    LOGMSG_WRN("Hello from: %s\n", m_whoAmI.c_str());
    LOGMSG_MSG("Hello from: %s\n", m_whoAmI.c_str());
    LOGMSG_ERR("Hello from: %s\n", m_whoAmI.c_str());

    LOGMSG_MSG_S() << std::endl;
    LOGMSG_MSG_S() << "=================================" << std::endl;
    LOGMSG_MSG_S() << std::endl;

    LOGMSG_DBG_S() << "Hello from: " << m_whoAmI << std::endl;
    LOGMSG_WRN_S() << "Hello from: " << m_whoAmI << std::endl;
    LOGMSG_MSG_S() << "Hello from: " << m_whoAmI << std::endl;
    LOGMSG_ERR_S() << "Hello from: " << m_whoAmI << std::endl;
}
