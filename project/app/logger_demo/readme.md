## Start dev container
```bash
$ cd ./dockerEnv/dev
$ ./start_dev_container.sh
```

## Example
```bash
$ cd ./debug
$ ./app/logger_demo/logger_demo

20230414_015156_072 [DBG]                      main:    17,    27,Hello World from app: logger_demo
20230414_015156_074 [WRN]                      main:    18,    27,Hello World from app: logger_demo
20230414_015156_074 [MSG]                      main:    19,    27,Hello World from app: logger_demo
20230414_015156_074 [ERR]                      main:    20,    27,Hello World from app: logger_demo
20230414_015156_074 [MSG]                      main:    22,    27,
20230414_015156_074 [MSG]                      main:    23,    27,=================================
20230414_015156_074 [MSG]                      main:    24,    27,
20230414_015156_074 [DBG]                      main:    26,    27,Hello World from app: logger_demo
20230414_015156_074 [WRN]                      main:    27,    27,Hello World from app: logger_demo
20230414_015156_074 [MSG]                      main:    28,    27,Hello World from app: logger_demo
20230414_015156_074 [ERR]                      main:    29,    27,Hello World from app: logger_demo
20230414_015156_074 [MSG]                      main:    31,    27,
20230414_015156_074 [MSG]                      main:    32,    27,=================================
20230414_015156_074 [MSG]                      main:    33,    27,
20230414_015156_074 [DBG]                     Dummy::                   WhoAmI:    16,    27,Hello from: dummy_001
20230414_015156_074 [WRN]                     Dummy::                   WhoAmI:    17,    27,Hello from: dummy_001
20230414_015156_074 [MSG]                     Dummy::                   WhoAmI:    18,    27,Hello from: dummy_001
20230414_015156_074 [ERR]                     Dummy::                   WhoAmI:    19,    27,Hello from: dummy_001
20230414_015156_074 [MSG]                     Dummy::                   WhoAmI:    21,    27,
20230414_015156_074 [MSG]                     Dummy::                   WhoAmI:    22,    27,=================================
20230414_015156_074 [MSG]                     Dummy::                   WhoAmI:    23,    27,
20230414_015156_074 [DBG]                     Dummy::                   WhoAmI:    25,    27,Hello from: dummy_001
20230414_015156_074 [WRN]                     Dummy::                   WhoAmI:    26,    27,Hello from: dummy_001
20230414_015156_074 [MSG]                     Dummy::                   WhoAmI:    27,    27,Hello from: dummy_001
20230414_015156_074 [ERR]                     Dummy::                   WhoAmI:    28,    27,Hello from: dummy_001
```
