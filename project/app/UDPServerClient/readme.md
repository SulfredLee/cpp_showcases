## Start dev container
```bash
$ cd ./dockerEnv/dev
$ ./start_dev_container.sh
```

## Example
```bash
$ ./app/UDPServerClient/UDPServerClient
20230414_103840_989 [MSG]              ClientWorker:    72, 83461,Send success! count: 2000
20230414_103840_990 [MSG]              ServerWorker:    34, 83456,Received count: 2000 byteRecv: 4
20230414_103840_990 [MSG]              ServerWorker:    41, 83456,Send success! count: 0
20230414_103840_990 [MSG]              ClientWorker:    86, 83461,Received count: 0
20230414_103841_990 [MSG]              ClientWorker:    72, 83461,Send success! count: 2001
20230414_103841_990 [MSG]              ServerWorker:    34, 83456,Received count: 2001 byteRecv: 4
20230414_103841_990 [MSG]              ServerWorker:    41, 83456,Send success! count: 1
20230414_103841_990 [MSG]              ClientWorker:    86, 83461,Received count: 1
20230414_103842_990 [MSG]              ClientWorker:    72, 83461,Send success! count: 2002
20230414_103842_990 [MSG]              ServerWorker:    34, 83456,Received count: 2002 byteRecv: 4
20230414_103842_990 [MSG]              ServerWorker:    41, 83456,Send success! count: 2
20230414_103842_990 [MSG]              ClientWorker:    86, 83461,Received count: 2
20230414_103843_991 [MSG]              ClientWorker:    72, 83461,Send success! count: 2003
20230414_103843_991 [MSG]              ServerWorker:    34, 83456,Received count: 2003 byteRecv: 4
20230414_103843_991 [MSG]              ServerWorker:    41, 83456,Send success! count: 3
20230414_103843_991 [MSG]              ClientWorker:    86, 83461,Received count: 3
20230414_103844_991 [MSG]              ClientWorker:    72, 83461,Send success! count: 2004
20230414_103844_991 [MSG]              ServerWorker:    34, 83456,Received count: 2004 byteRecv: 4
20230414_103844_991 [MSG]              ServerWorker:    41, 83456,Send success! count: 4
20230414_103844_991 [MSG]              ClientWorker:    86, 83461,Received count: 4
```
