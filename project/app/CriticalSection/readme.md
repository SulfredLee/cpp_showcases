## Start dev container
```bash
$ cd ./dockerEnv/dev
$ ./start_dev_container.sh
```

## Example
```bash
$ ./app/CriticalSection/CriticalSection
20230415_023806_010 [MSG]                   FooLoop:    13,   172,count: 0
20230415_023806_010 [MSG]                   FooLoop:    13,   172,count: 1
20230415_023806_010 [MSG]                   FooLoop:    13,   172,count: 2
20230415_023806_010 [MSG]                   FooLoop:    13,   172,count: 3
20230415_023806_010 [MSG]                   FooLoop:    13,   172,count: 4
20230415_023806_010 [MSG]                   FooLoop:    13,   172,count: 5
20230415_023806_010 [MSG]                   FooLoop:    13,   172,count: 6
20230415_023806_010 [MSG]                   FooLoop:    13,   172,count: 7
20230415_023806_010 [MSG]                   FooLoop:    13,   172,count: 8
20230415_023806_010 [MSG]                   FooLoop:    13,   172,count: 9
```
