#include <iostream>

#include "Logger.h"
#include "CriticalSection.h"

CriticalSection section;

void FooLoop(int thisCount)
{
    CriticalLock lock(section);
    if (thisCount < 10)
    {
        LOGMSG_MSG_C("count: %d\n", thisCount++);
        FooLoop(thisCount);
    }
    else
        return;
}

int main(int argc, char *argv[])
{
    FooLoop(0);

    return 0;
}
