#include <iostream>
#include <thread>
#include <string.h>
#include <unistd.h>

#include "MsgQ_C.h"

MsgQ_C* queue = createMsgQ_C();

void thread001Fun()
{
    int i = 0;
    while (true)
    {
        char* string = new char[1024];
        sprintf(string, "thread 001 add number %03d", i++);
        pushMsgQ_C((void**)&string, queue);
        usleep(500000); // sleep 0.5 sec
    }
}

void thread002Fun()
{
    int i = 1000;
    while (true)
    {
        char* string = new char[1024];
        sprintf(string, "thread 002 add number %03d", i++);
        pushMsgQ_C((void**)&string, queue);
        usleep(1000000); // sleep 1 sec
    }
}

int main(int argc, char* argv[])
{
    std::thread thread001(thread001Fun);
    std::thread thread002(thread002Fun);
    int count = 0;
    while (count < 10)
    {
        char* string;
        getMsgQ_C((void**)&string, queue);
        printf("Receive message: %s\n", string);
        delete string;
        count++;
    }

    freeMsgQ_C(queue);
    return 0;
}
