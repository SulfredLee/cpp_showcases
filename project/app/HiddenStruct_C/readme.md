## Start dev container
```bash
$ cd ./dockerEnv/dev
$ ./start_dev_container.sh
```

## Example
```bash
$ ./app/HiddenStruct_C/HiddenStruct_C
thread 002 add number 1000
thread 001 add number 000
thread 001 add number 001
thread 002 add number 1001
thread 001 add number 002
thread 001 add number 003
thread 002 add number 1002
thread 001 add number 004
thread 001 add number 005
thread 002 add number 1003
```
