## C++17 STL Cookbook

### Content
| topic                   | recipe                                                       | file      | key_value                                                                              |
|-------------------------|:------------------------------------------------------------:|:---------:|:--------------------------------------------------------------------------------------:|
| New C++17 features      | Using structured bindings to unpack bundled return values    | Ch01_01.h | auto can replace std:tie in C++17 to decompose tuple and pair                          |
|                         | Limiting variable scopes to if and switch statements         | Ch01_02.h | no need to use extra braces, syntax sugar in C++17 to limit variable scope             |
|                         | Profiting from the new bracket initializer rules             | Ch01_03.h | C++17 made it harder to accidentally define an initializer list when using auto        |
|                         | Constructor automatically deduce the template class type     | Ch01_04.h | example for ... operator, std::common_type_t pick common type without information loss |
|                         | Simplifying compile time decisions with constexpr-if         | Ch01_05.h | example for std::is_same_v                                                             |
|                         | Enabling header-only libraries with inline variables         | Ch01_06.h | resolve multiple definition in one header hpp, and multiple cpp files                  |
|                         | Implementing handy helper functions with fold expressions    | Ch01_07.h | example for ... operator, fold expressions                                             |
| STL Containers          | Using the erase-remove idiom on std::vector                  | Ch02_01.h | examples showing how to use remove() and remove_if()                                   |
|                         | Deleting items from an unsorted std::vector in O(1) time     | Ch02_02.h | delete item in the middle and move the last item to replace it                         |
|                         | Accessing std::vector instances the fast or the safe way     | Ch02_03.h | examples showing .at() or [] operator                                                  |
|                         | Keeping std::vector instances sorted                         | Ch02_04.h | lower_bound use case                                                                   |
|                         | Inserting items efficiently and conditionally into std::map  | Ch02_05.h | try_emplace with std::map                                                              |
|                         | Knowing the new insertion hint semantics of std::map::insert | Ch02_06.h | insertion into map with hint                                                           |
|                         | Efficiently modifying the keys of std::map items             | Ch02_07.h | modify key in map                                                                      |
|                         | Using std::unordered_map with custom types                   | Ch02_08.h | apply std::hash to custom types                                                        |
|                         | Filter unique user input                                     | Ch02_09.h | use std::set to do this                                                                |
|                         | Implement simple rpn calculator with stack                   | Ch02_10.h | stack                                                                                  |
|                         | Word frequency counter                                       | Ch02_11.h | use std::map                                                                           |
|                         | Tool for finding very long sentences                         | Ch02_12.h | use std::multimap and std::count to count number of words in sentence                  |
|                         | Personal to-do list                                          | Ch02_13.h | std::priority_queue                                                                    |
| Iterators               | Your own iterable range                                      | Ch03_01.h | iterable range can be used in --- for (int i : your_range)                             |
|                         | Your own iterators compatible with STL                       | Ch03_02.h | std::minmax_element, std::forward_iterator_tag                                         |
|                         | Iterator adapters                                            | Ch03_03.h | std::back_inserter(), std::front_inserter(), std::inserter()                           |
|                         | Algorithms with iterators                                    | Ch03_04.h | Fibonacci implementation                                                               |
|                         | Reverse iterator                                             | Ch03_05.h |                                                                                        |
|                         | Iterable sentinels                                           | Ch03_06.h | iterating range without knowing end poisition ahead                                    |
|                         | Debug iterators                                              | Ch03_07.h | dangling dereference checking                                                          |
|                         | Zip iterators                                                | Ch03_08.h | custom zip iterators integrate with std algorithm                                      |
| Lambda                  | Defining functions on the run                                | Ch04_01.h | showcases basic usage of lambda                                                        |
|                         | Wrapping lambdas into std::funtion                           | Ch04_02.h | adding polymorphy with lambdas                                                         |
|                         | Concatenation of funtions                                    | Ch04_03.h | lambda function with ... operator, fold expressions                                    |
|                         | Creating complex predicates                                  | Ch04_04.h | std::logical_and<>{}, std::copy_if, and customer bool function                         |
|                         | Calling multiple functions with the same input               | Ch04_05.h | put a list of functions in std::initializer_list<int>                                  |
|                         | Implementing transform_if                                    | Ch04_06.h | using std::accumulate and lambdas                                                      |
|                         | Cartesian product pairs                                      | Ch04_07.h | calculation in compile time                                                            |
| STL Algorithms          | Copying items between containers                             | Ch05_01.h |                                                                                        |
|                         | Sorting containers                                           | Ch05_02.h | sort, shuffle, partial_sort, custom_sort                                               |
|                         | Removing specific items from containers                      | Ch05_03.h | remove, remove_if, remove_copy_if, copy_if                                             |
|                         | Transforming the contents of containers                      | Ch05_04.h | transform, copy                                                                        |
|                         | Finding items in ordered and unordered vectors               | Ch05_05.h | find, find_if, binary_search, equal_range, upper_bound, lower_bound                    |
|                         | Limiting the values of a vector to a specific numeric range  | Ch05_06.h | clamp                                                                                  |
|                         | Locating patterns in strings                                 | Ch05_07.h | std::search, default_searcher, boyer_moore_searcher, boyer_moore_horspool_searcher     |
|                         | Sampling large vectors                                       | Ch05_08.h | std::sample                                                                            |
|                         | Generating permutations of input sequences                   | Ch05_09.h | std::next_permutations                                                                 |
|                         | Implementing a dictionary merging tool                       | Ch05_10.h | std::merg                                                                              |
| Advanced STL Algorithms | Implementing a trie class using STL algorithms               | Ch06_01.h |                                                                                        |
|                         | A search input suggestion                                    | Ch06_02.h | trie, read from file, read from input                                                  |
|                         | Fourier transform formula with STL                           | Ch06_03.h |                                                                                        |
|                         | Error sum of two vectors                                     | Ch06_04.h | std::inner_product                                                                     |
|                         | ASCII Mandelbrot renderer                                    | Ch06_05.h |                                                                                        |
|                         | Split function                                               | Ch06_06.h |                                                                                        |
|                         | gather                                                       | Ch06_07.h |                                                                                        |
|                         | Removing consecutive whitespace                              | Ch06_08.h |                                                                                        |
|                         | String compression                                           | Ch06_09.h |                                                                                        |
| String, Stream class    | Create, concatenating, transforming                          | Ch07_01.h | std::transform                                                                         |
|                         | Trimming whitespace                                          | Ch07_02.h | std::string find_first_not_of find_last_not_of                                         |
|                         | The good o string_view                                       | Ch07_03.h | std::string_view                                                                       |
|                         |                                                              |           |                                                                                        |


### Reference

[github web book](https://github.com/PacktPublishing/Cpp17-STL-Cookbook/tree/master)
