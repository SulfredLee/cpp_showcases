#ifndef CH04_03_H
#define CH04_03_H

#include <iostream>
#include <functional>

template <typename T, typename ...Ts>
    auto concat(T t, Ts ...ts)
{
    if constexpr (sizeof...(ts) > 0) {
            return [=](auto ...parameters) { return t(concat(ts...)(parameters...)); };
        } else  {
        return t;
    }
}

int Ch04_03_Main(int argc, char *argv[])
{
    auto twice  ([] (int i) { return i * 2; });
    auto thrice ([] (int i) { return i * 3; });

    auto combined (concat(twice, thrice, std::plus<int>{}));

    // if you give concat(f, g, h), then the combined will give f(g(h()))
    // 2 * 3 * (2 + 3) = 30
    std::cout << combined(2, 3) << '\n';

    return 0;
}

#endif
