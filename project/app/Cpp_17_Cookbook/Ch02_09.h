#ifndef CH02_09_H
#define CH02_09_H

#include <iostream>
#include <set>
#include <string>
#include <iterator> // for ostream_iterator

using namespace std;

int Ch02_09_Main()
{
    set<string> s;

    istream_iterator<string> it {cin};
    istream_iterator<string> end;

    copy(it, end, inserter(s, s.end()));

    for (const auto word : s) {
        cout << word << ", ";
    }
    cout << '\n';

    return 0;
}

#endif
