#ifndef CH04_05_H
#define CH04_05_H

#include <iostream>

template <typename ... Ts>
static auto multicall (Ts ...functions)
{
    return [=](auto x) {
        (void)std::initializer_list<int>{
            // this is a syntax trick to put the functions into the list
            ((void)functions(x), 0)...
        };
    };
}

template <typename F, typename ... Ts>
    static auto for_each (F f, Ts ...xs) {
    // this is a syntax trick to put the functions into the list
    (void)std::initializer_list<int>{
        ((void)f(xs), 0)...
            };
}

static auto brace_print (char a, char b) {
    return [=] (auto x) {
        std::cout << a << x << b << ", ";
    };
}

int Ch04_05_Main(int argc, char *argv[])
{
    auto f  (brace_print('(', ')'));
    auto g  (brace_print('[', ']'));
    auto h  (brace_print('{', '}'));
    auto nl ([](auto) { std::cout << '\n'; });

    auto call_fgh (multicall(f, g, h, nl));

    for_each(call_fgh, 1, 2, 3, 4, 5);

    return 0;
}

#endif
