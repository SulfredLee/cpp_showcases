// https://github.com/PacktPublishing/Cpp17-STL-Cookbook/tree/master
#include <iostream>
// #include "Ch01_01.h"
// #include "Ch01_02.h"
// #include "Ch01_03.h"
// #include "Ch01_04.h"
// #include "Ch01_05.h"
// #include "Ch01_06.h"
// #include "Ch01_07.h"
// #include "Ch02_01.h"
// #include "Ch02_02.h"
// #include "Ch02_03.h"
// #include "Ch02_04.h"
// #include "Ch02_05.h"
// #include "Ch02_06.h"
// #include "Ch02_07.h"
// #include "Ch02_08.h"
// #include "Ch02_09.h"
// #include "Ch02_10.h"
// #include "Ch02_11.h"
// #include "Ch02_12.h"
// #include "Ch02_13.h"
// #include "Ch03_01.h"
// #include "Ch03_02.h"
// #include "Ch03_03.h"
// #include "Ch03_04.h"
// #include "Ch03_05.h"
// #include "Ch03_06.h"
// #include "Ch03_07.h"
// #include "Ch03_08.h"
// #include "Ch04_01.h"
// #include "Ch04_02.h"
// #include "Ch04_03.h"
// #include "Ch04_04.h"
// #include "Ch04_05.h"
// #include "Ch04_06.h"
// #include "Ch04_07.h"
// #include "Ch05_01.h"
// #include "Ch05_02.h"
// #include "Ch05_03.h"
// #include "Ch05_04.h"
// #include "Ch05_05.h"
// #include "Ch05_06.h"
// #include "Ch05_07.h"
// #include "Ch05_08.h"
// #include "Ch05_09.h"
// #include "Ch05_10.h"
// #include "Ch06_01.h"
// #include "Ch06_02.h"
// #include "Ch06_03.h"
// #include "Ch06_04.h"
// #include "Ch06_05.h"
// #include "Ch06_06.h"
// #include "Ch06_07.h"
// #include "Ch06_08.h"
// #include "Ch06_09.h"
// #include "Ch07_01.h"
// #include "Ch07_02.h"
#include "Ch07_03.h"

int main (int argc, char *argv[])
{
    std::cout << "Hello World from app: Cpp_17_Cookbook" << std::endl;

    // return Ch01_01_Main();
    // return Ch01_02_Main();
    // return Ch01_03_Main();
    // return Ch01_04_Main();
    // return Ch01_05_Main();
    // return Ch01_06_Main();
    // return Ch01_07_Main();
    // return Ch02_01_Main();
    // return Ch02_02_Main();
    // return Ch02_03_Main();
    // return Ch02_04_Main();
    // return Ch02_05_Main();
    // return Ch02_06_Main();
    // return Ch02_07_Main();
    // return Ch02_08_Main();
    // return Ch02_09_Main();
    // return Ch02_10_Main();
    // return Ch02_11_Main();
    // return Ch02_12_Main();
    // return Ch02_13_Main();
    // return Ch03_01_Main();
    // return Ch03_02_Main();
    // return Ch03_03_Main();
    // return Ch03_04_Main();
    // return Ch03_05_Main();
    // return Ch03_06_Main(argc, argv);
    // return Ch03_07_Main(argc, argv);
    // return Ch03_08_Main(argc, argv);
    // return Ch04_01_Main(argc, argv);
    // return Ch04_02_Main(argc, argv);
    // return Ch04_03_Main(argc, argv);
    // return Ch04_04_Main(argc, argv);
    // return Ch04_05_Main(argc, argv);
    // return Ch04_06_Main(argc, argv);
    // return Ch04_07_Main(argc, argv);
    // return Ch05_01_Main(argc, argv);
    // return Ch05_02_Main(argc, argv);
    // return Ch05_03_Main(argc, argv);
    // return Ch05_04_Main(argc, argv);
    // return Ch05_05_Main(argc, argv);
    // return Ch05_06_Main(argc, argv);
    // return Ch05_07_Main(argc, argv);
    // return Ch05_08_Main(argc, argv);
    // return Ch05_09_Main(argc, argv);
    // return Ch05_10_Main(argc, argv);
    // return Ch06_01_Main(argc, argv);
    // return Ch06_02_Main(argc, argv);
    // return Ch06_03_Main(argc, argv);
    // return Ch06_04_Main(argc, argv);
    // return Ch06_05_Main(argc, argv);
    // return Ch06_06_Main(argc, argv);
    // return Ch06_07_Main(argc, argv);
    // return Ch06_08_Main(argc, argv);
    // return Ch06_09_Main(argc, argv);
    // return Ch07_01_Main(argc, argv);
    // return Ch07_02_Main(argc, argv);
    return Ch07_03_Main(argc, argv);

    return 0;
}
