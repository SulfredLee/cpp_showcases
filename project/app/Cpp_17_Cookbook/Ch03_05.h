#ifndef CH03_05_H
#define CH03_05_H

#include <iostream>
#include <list>
#include <iterator>

using namespace std;

int Ch03_05_Main()
{
    list<int> l {1, 2, 3, 4, 5};

    copy(l.rbegin(), l.rend(), ostream_iterator<int>{cout, ", "});
    std::cout << '\n';

    // use iterator adapters
    copy(make_reverse_iterator(end(l)),
         make_reverse_iterator(begin(l)),
         ostream_iterator<int>{cout, ", "});
    std::cout << '\n';

    return 0;
}

#endif
