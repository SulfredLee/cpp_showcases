#ifndef CH05_09_H
#define CH05_09_H

#include <iostream>
#include <vector>
#include <string>
#include <iterator>
#include <algorithm>

using namespace std;

/*echo "1 2 3 4" | ./app/Cpp_17_Cookbook/Cpp_17_Cookbook*/
int Ch05_09_Main(int argc, char *argv[])
{
    vector<string> v {istream_iterator<string>{cin}, {}};
    sort(begin(v), end(v));

    do {
        copy(begin(v), end(v), ostream_iterator<string>{cout, ", "});
        cout << '\n';
    } while (next_permutation(begin(v), end(v)));
    /*1, 2, 3, 4,
      1, 2, 4, 3,
      1, 3, 2, 4,
      1, 3, 4, 2,
      1, 4, 2, 3,
      1, 4, 3, 2,
      2, 1, 3, 4,
      2, 1, 4, 3,
      2, 3, 1, 4,
      2, 3, 4, 1,
      2, 4, 1, 3,
      2, 4, 3, 1,
      3, 1, 2, 4,
      3, 1, 4, 2,
      3, 2, 1, 4,
      3, 2, 4, 1,
      3, 4, 1, 2,
      3, 4, 2, 1,
      4, 1, 2, 3,
      4, 1, 3, 2,
      4, 2, 1, 3,
      4, 2, 3, 1,
      4, 3, 1, 2,
      4, 3, 2, 1,*/

    return 0;
}


#endif
