#ifndef CH05_03_H
#define CH05_03_H

#include <iostream>
#include <algorithm>
#include <vector>
#include <iterator>
#include <random>

using namespace std;

static void print(const vector<int> &v)
{
    copy(begin(v), end(v), ostream_iterator<int>{cout, ", "});
    cout << '\n';
}

int Ch05_03_Main(int argc, char *argv[])
{
    vector<int> v {1, 2, 3, 4, 5, 6};

    print(v); // 1, 2, 3, 4, 5, 6,

    {
        const auto new_end (remove(begin(v), end(v), 2));
        v.erase(new_end, end(v));
    }
    print(v); // 1, 3, 4, 5, 6,

    {
        auto odd_number ([](int i) { return i % 2 != 0; });
        const auto new_end (remove_if(begin(v), end(v), odd_number));
        v.erase(new_end, end(v));
    }
    print(v); // 4, 6,

    replace(begin(v), end(v), 4, 123);
    print(v); // 123, 6,

    v = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    vector<int> v2;
    vector<int> v3;

    auto odd_number  ([](int i) { return i % 2 != 0; });
    auto even_number ([](int i) { return i % 2 == 0; });

    remove_copy_if(begin(v), end(v), back_inserter(v2), odd_number);
    copy_if(       begin(v), end(v), back_inserter(v3), even_number);

    print(v2); // 2, 4, 6, 8, 10,
    print(v3); // 2, 4, 6, 8, 10,

    return 0;
}

#endif
