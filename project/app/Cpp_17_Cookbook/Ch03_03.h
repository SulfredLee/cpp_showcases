#ifndef CH03_03_H
#define CH03_03_H

#include <iostream>
#include <string>
#include <iterator>
#include <sstream>
#include <deque>

using namespace std;

int Ch03_03_Main()
{
    // iterator adapters: std::back_inserter(), std::front_inserter(), std::inserter()

    istream_iterator<int> it_cin {cin};
    istream_iterator<int> end_cin;

    deque<int> v;

    // insert in the back from user input
    copy(it_cin, end_cin, back_inserter(v));
    // wrap deque into std::back_insert_iterator by std::back_inserter
    // std::back_insert_iterator will do v.push_back(item)

    istringstream sstr {"123 456 789"};
    auto deque_middle (next(begin(v), static_cast<int>(v.size()) / 2));

    // insert in the middle
    copy(istream_iterator<int>{sstr}, {}, inserter(v, deque_middle));

    initializer_list<int> il2 {-1, -2, -3};
    // insert in the front
    copy(begin(il2), end(il2), front_inserter(v));

    // output to command line
    copy(begin(v), end(v), ostream_iterator<int>{cout, ", "});
    cout << '\n';

    return 0;
}

#endif
