#ifndef CH02_01_H
#define CH02_01_H

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int Ch02_01_Main()
{
    vector<int> v {1, 2, 3, 2, 5, 2, 6, 2, 4, 8};

    {
        const auto new_end (remove(begin(v), end(v), 2)); // remove 2 from vector
        v.erase(new_end, end(v));
    }

    for (auto i : v) {
        cout << i << ", ";
    }
    cout << '\n';

    {
        const auto odd ([](int i) { return i % 2 != 0; });
        const auto new_end (remove_if(begin(v), end(v), odd)); // remove odd value from vector
        v.erase(new_end, end(v));
    }

    v.shrink_to_fit(); // change the capicity of the vector to match the size of the vector

    for (auto i : v) {
        cout << i << ", ";
    }
    cout << '\n';

    return 0;
}

#endif
