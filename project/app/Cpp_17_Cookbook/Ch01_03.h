#ifndef CH01_03_H
#define CH01_03_H
#include <vector>
#include <iostream>

template<typename T>
void Print_Vec(const T& vv)
{
    for (const auto& ele : vv)
    {
        std::cout << ele << " ";
    }
    std::cout << std::endl;
}

int Ch01_03_Main()
{
    // initializer syntax without auto
    int x1 = 1;
    int x2 {1};
    int x3 (1);
    std::cout << x1 << " " << x2 << " " << x3 << std::endl;

    std::vector<int> v1 {1, 2, 3}; // Vector with three ints
    std::vector<int> v2 = {1, 2, 3}; // same here
    std::vector<int> v3 (10, 20); // Vector with 10 ints, each have value 20
    Print_Vec(v1);
    Print_Vec(v2);
    Print_Vec(v3);

    // initializer syntax with auto
    auto v {1}; //  v is int *** before C++17, some compiler will create a std::initializer_list<int> ***
    // auto w {1, 2}; // error: only single elements can be used

    auto x = {1}; // x is std::initializer_list<int>
    auto y = {1, 2}; // y is std::initializer_list<int>
    // auto z = {1, 2, 3.0}; // error: Cannot deduce element type

    std::cout << v << " " << std::endl;
    Print_Vec(x);
    Print_Vec(y);

    return 0;
}

#endif
