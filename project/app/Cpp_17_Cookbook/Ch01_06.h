#ifndef CH01_06_H
#define CH01_06_H

#include <string>
#include <stdio.h>

struct C {
#if 1
    static const inline std::string N {"abc"};
};
#else
    static std::string& N() {
        static std::string s {"abc"};
        return s;
    }
};
#endif

inline C example_c;

int Ch01_06_Main()
{
    printf("%s\n", example_c.N.c_str());

    return 0;
}

#endif
