#ifndef CH03_07_H
#define CH03_07_H

#include <iostream>
#include <vector>

using namespace std;

int Ch03_07_Main(int argc, char *argv[])
{
    vector<int> v {1, 2, 3};
    v.shrink_to_fit(); // ensure that its capacity is really 3

    const auto it (begin(v));

    cout << *it << endl;

    v.push_back(123);

    // Detected by GLIBC++ debug mode
    // or with llvm with activated -fsanitize=address -fsanitize=undefined

    // there is a memory reallocation of the vector, the following dereferencing should fail
    // but this behavior is undefined, unless you add extra building flag -D_GLIBCXX_DEBUG
    cout << *it << endl;

    return 0;
}

#endif
