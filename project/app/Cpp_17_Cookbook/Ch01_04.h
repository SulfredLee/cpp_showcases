#ifndef CH01_04_H
#define CH01_04_H
#include <tuple>
#include <string>
#include <type_traits>
#include <iostream>

template <typename T1, typename T2, typename T3>
    class my_wrapper {
    T1 t1;
    T2 t2;
    T3 t3;

 public:
 my_wrapper(T1 t1_, T2 t2_, T3 t3_)
     : t1{t1_}, t2{t2_}, t3{t3_}
    {}

    /* … */
};

template <typename T>
struct sum {
    T value;

    template <typename ... Ts>
    sum(Ts&& ... values) : value{(values + ...)} {}
};

template <typename ... Ts>
sum(Ts&& ... ts) -> sum<std::common_type_t<Ts...>>; // figure out which type fits all the values without information loss

int Ch01_04_Main()
{
    my_wrapper wrap (123, 4.56, std::string{"foo"});

    std::pair  pair  (123, std::string{"string"});
    std::tuple tuple (123, 4.56, std::string{"string"});

    sum s1         {1u, 2.1, 3, 4.0f};
    sum s2         (1u, 2.1, 3, 4.0f);
    sum string_sum {std::string{"abc"}, "def"};

    std::cout << s1.value         << '\n'
              << s2.value         << '\n'
              << string_sum.value << '\n';

    return 0;
}

#endif
