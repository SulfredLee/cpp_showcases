#ifndef CH07_01_H
#define CH07_01_H

#include <iostream>
#include <string>
#include <string_view>
#include <sstream>
#include <algorithm>

using namespace std;

int Ch07_01_Main(int argc, char *argv[])
{
    string a { "a"  };
    auto   b ( "b"s ); // s == string

    string_view c { "c"   };
    auto        d ( "d"sv ); // sv == string_view

    cout << a << ", " << b << '\n';
    cout << c << ", " << d << '\n';

    cout << a + b << '\n';
    cout << a + string{c} << '\n'; // need to change back to string since string_view has no zero-terminate

    ostringstream o;

    o << a << " " << b << " " << c << " " << d;
    auto concatenated (o.str());

    cout << concatenated << '\n'; // a b c d

    transform(begin(concatenated), end(concatenated), begin(concatenated), ::toupper);
    cout << concatenated << '\n'; // A B C D

    return 0;
}

#endif
