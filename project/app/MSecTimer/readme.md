## Start dev container
```bash
$ cd ./dockerEnv/dev
$ ./start_dev_container.sh
```

## Example
```bash
$ ./app/MSecTimer/MSecTimer
20230415_042948_825 [MSG]                       Foo:    28,   293,Print from outside 10
20230415_042949_825 [MSG]                       Foo:    28,   293,Print from outside 10
20230415_042950_825 [MSG]                       Foo:    28,   293,Print from outside 10
20230415_042951_825 [MSG]                       Foo:    28,   293,Print from outside 10
20230415_042953_825 [MSG]                  FooClass::                    Print:    22,   294,Print from class 10
20230415_042954_826 [MSG]                  FooClass::                    Print:    22,   294,Print from class 10
20230415_042955_826 [MSG]                  FooClass::                    Print:    22,   294,Print from class 10
20230415_042956_826 [MSG]                  FooClass::                    Print:    22,   294,Print from class 10
```
