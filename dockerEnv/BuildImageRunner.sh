
#!/bin/bash

DOCKER_VERSION=$(grep "DOCKER_RUNNER_VERSION:" ../.gitlab-ci.yml | cut -c 26-)
docker build --file Dockerfile.Run --target runner -t cpp_showcases:${DOCKER_VERSION} ..